import React, { useEffect, useState } from 'react';
import { Input, AutoComplete, Image, Row, Col } from 'antd';
import { formatCash } from 'utils';

const InputSearch = (props) => {
  const [options, setOptions] = useState([]);
  const productList = props.productList;
  const onSelectProduct = props.onSelectProduct;

  useEffect(() => {
    const options = productList?.map(item => ({
      value: item.productName,
      label: (
        <Row gutter={[16, 16]} align="middle">
          <Col span={7}>
            <Image
              style={{
                maxWidth: 120,
              }}
              src={item.imageProduct} />
          </Col>
          <Col span={16}>
            <Row justify="center">
              <Col span={24}>
                <h3>{item.productName}</h3>
              </Col>
              <Col span={24}>
                <p>{formatCash(item.price)}</p>
              </Col>
            </Row>
          </Col>
        </Row>
      )
    }))
    setOptions(options);
  }, [productList])

  return (
    <AutoComplete
      allowClear
      dropdownMatchSelectWidth={252}
      style={{
        width: "100%"
      }}
      options={options}
      onSelect={onSelectProduct}
      // onSearch={handleSearch}
      filterOption={(inputValue, option) =>
        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
      }
    >
      <Input.Search size="large" placeholder="Gõ tên sản phẩm..." enterButton />
    </AutoComplete>


  );
};

export default InputSearch
