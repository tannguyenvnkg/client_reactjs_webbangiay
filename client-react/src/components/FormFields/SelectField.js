import { Select } from 'antd';
import React from 'react'
import { useController } from 'react-hook-form'

export function SelectField({ name, control, label, disabled, options, ...inputProps }) {
    const {
        field: { value, onChange, onBlur },
        fieldState: { invalid, error },
    } = useController({
        name,
        control,
    })
    const { Option } = Select;

    return (
        <Select
            value={value}
            onChange={onChange}
            onBlur={onBlur}
        >
            {options.map((option) => (
                <Option
                    key={option.value}
                    value={option.value}
                >
                    {option.label}
                </Option>
            ))}
        </Select>
    )
}
