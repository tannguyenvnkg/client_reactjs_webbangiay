import { TextField } from '@mui/material'
import { Input, InputNumber } from 'antd'
import React from 'react'
import { useController } from 'react-hook-form'

export function AreaField({
    name, control, label, placeholder, type, disabled, autoSize, ...inputProps }) {
    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error, }
    } = useController({
        name,
        control,
    })

    const { TextArea } = Input;

    return (
        <TextArea
        autoSize={autoSize}
            disabled={disabled}
            placeholder={placeholder}
            size="middle"
            value={value}
            onChange={onChange}
            onBlur={onBlur}
        />
    )
}
