import React from 'react'
import { useController } from 'react-hook-form'

export function UploadFile({ 
    name, 
    control, 
    onChange, 
    disabled, ...props }) {
    const {
        field: { value, onBlur, ref },
        fieldState: { invalid, error },
    } = useController({
        name,
        control,
    })
    return (
    <input 
        type="file"
        name={name}
        onChange={onChange}
        {...props}
    />
  )
}
