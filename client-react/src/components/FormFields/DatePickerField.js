import { TextField } from '@mui/material'
import { DatePicker, Input, InputNumber } from 'antd'
import React from 'react'
import { useController } from 'react-hook-form'
import moment from 'moment';

export function DatePickerField({
    name, control, label, placeholder, type, disabled, bordered, ...props }) {
    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error, }
    } = useController({
        name,
        control,
    })
    const dateFormat = 'DD-MM-YYYY';
    // const moment = moment(value).format(dateFormat)
    return (
        <DatePicker
            bordered={bordered}
            disabled={disabled}
            placeholder={placeholder}
            defaultValue={moment(value)}
            format={(value) => value.format(dateFormat)}
            onChange={onChange}
            onBlur={onBlur}
            {...props}
        />
    )
}
