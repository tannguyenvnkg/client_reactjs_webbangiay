import { Button, Upload } from 'antd'
import React from 'react'
import { useController } from 'react-hook-form'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

export function UploadField({ 
    name, 
    control, 
    action, 
    disabled, 
    customRequest, 
    onPreview,
    onChange,
    ...props }) {
    const {
        field: { value, onBlur },
        fieldState: { invalid, error },
    } = useController({
        name,
        control,
    })
    return (
        <Upload
            name={name}
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            customRequest={customRequest}
            onChange={onChange}
            disabled={disabled}
            props={props}
            onPreview={onPreview}
            accept=".png,.jpg"
        >
            {value ? <img src={value} alt={name} style={{ width: '100%' }} /> : <PlusOutlined />}
            
        </Upload>
    )
}
