import { InputNumber } from 'antd';
import React from 'react';
import { useController } from 'react-hook-form';

export function ButtonGroupField({
    name,
    control,
    label,
    type,
    detailList,
    addonBefore,
    addonAfter,
    disabled,
    onClick,
    options,
    ...props }) {
    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error },
    } = useController({
        name,
        control,
    })
    return (
        <div>
            {detailList && detailList.map((item, inx) => (
                <InputNumber
                    key={item.size}
                    addonBefore={item.size}
                    onClick={() => onClick(item)}
                    value={item.quantityInStock}
                    onChange={(e) => onChange(e, item.size)}
                    addonAfter={addonAfter}
                />
            ))
            }
        </div>
    )
}
{/* <div>{item.size}</div>
                    <div>
                        <input
                            type='text'
                            value={item.quantityInStock}
                            onChange={(e) => handleQuantity(e, item.size)} />
                    </div> */}