import { TextField } from '@mui/material'
import { Input, InputNumber } from 'antd'
import React from 'react'
import { useController } from 'react-hook-form'

export function InputField({
    name, control, label, placeholder, type, disabled, bordered, ...props }) {
    const {
        field: { value, onChange, onBlur, ref },
        fieldState: { invalid, error, }
    } = useController({
        name,
        control,
    })


    return (
        <Input
            bordered={bordered}
            disabled={disabled}
            placeholder={placeholder}
            type={type}
            size="middle"
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            {...props}
        />
    )
}
