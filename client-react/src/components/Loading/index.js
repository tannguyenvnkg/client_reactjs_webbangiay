import { Spin } from 'antd';
import React from 'react'
import { LoadingOutlined } from '@ant-design/icons';

function Loading() {
    const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
    return (
        <div className="loading">
            <Spin indicator={antIcon} tip="Loading..." />
        </div>
    )
}

export default Loading