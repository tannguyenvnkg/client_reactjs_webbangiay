import {
  UserOutlined, FacebookFilled, TwitterSquareFilled, InstagramFilled
} from '@ant-design/icons';
import { Avatar } from 'antd';
import React from 'react';


export default function FooterContact() {
  return (
    <>
      <div className="footer-item__context">LIÊN HỆ CHÚNG TÔI</div>
      <div className="footer-item__body">
        <div className="body-item">
          <Avatar
            style={{
              backgroundColor: 'rgb(24 144 255)',
            }}
            className="item"
            shape="square"
            size="large"
            icon={<FacebookFilled />} />
          <div className="body-item__title">facebook.com</div>
        </div>

        <div className="body-item">
          <Avatar
            style={{
              backgroundColor: 'rgb(24 144 255)',
            }}
            className="item"
            shape="square"
            size="large"
            icon={<TwitterSquareFilled />} />
          <div className="body-item__title">twitter.com</div>
        </div>

        <div className="body-item">
          <Avatar
            style={{
              background: 'linear-gradient(to bottom right, #6600ff 0%, #ff6600 100%)'
            }}
            className="item"
            shape="square"
            size="large"
            icon={<InstagramFilled />} />
          <div className="body-item__title">instagram.com</div>
        </div>

      </div>
    </>
  )
}
