import {
    HomeOutlined, MailOutlined, ShopOutlined
} from '@ant-design/icons';
import { Col, Menu, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';

function HeaderNavbar() {
    let location = useLocation();

    const [keyMenu, setKeyMenu] = useState('')

    useEffect(() => {
        setKeyMenu(location.pathname);
    }, [location])

    return (
        <div className="header__navbar">
            <Row justify="center">
                <Col span={10}>
                    <Menu
                        mode="horizontal"
                        key="navbarMenu"
                        selectedKeys={keyMenu}
                        style={{ justifyContent: 'space-around' }}>
                        <Menu.Item key="/home" icon={<HomeOutlined />}>
                            <Link to="/home">TRANG CHỦ</Link>
                        </Menu.Item>
                        <Menu.Item key="/product_menu" icon={<ShopOutlined />}>
                            <Link to="/product_menu" >
                                DANH SÁCH SẢN PHẨM
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="/contact" icon={<MailOutlined />}>
                            <Link to="/contact">VỀ CHÚNG TÔI</Link>
                        </Menu.Item>
                    </Menu>
                </Col>
            </Row>

        </div>
    )
}

export default HeaderNavbar