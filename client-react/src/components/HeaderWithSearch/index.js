import {
  LoginOutlined,
  UserOutlined, WindowsOutlined,
  ShoppingCartOutlined
} from '@ant-design/icons';
import { Avatar, Badge, Button, Col, Dropdown, Menu, Modal, Row } from 'antd';
import { ACCESS_TOKEN, CART, CURRENT_USER } from 'constants/global';
import { authActions } from 'features/Auth/authSlice';
import { selectCartItems } from 'features/Cart/cartSlice';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import InputSearch from '../InputSearch';
import { logo } from 'constants/images'
import { selectProductList } from 'features/Home/homeSlice';
import { ProductDetails } from 'features/Product';


function HeaderWithSearch() {
  const { SubMenu } = Menu;
  const dispatch = useDispatch()
  let location = useLocation();

  const [keyMenu, setKeyMenu] = useState('')
  const [visible, setVisible] = useState();
  const [ product, setProduct] = useState(undefined)
  const [amountCart, setAmountCart] = useState(0)
  const cartItems = useSelector(selectCartItems)
  const productList = useSelector(selectProductList)

  const [visibleModel, setVisibleModel] = useState(false);

  
  const isLoggedIn = localStorage.getItem(ACCESS_TOKEN) !== null
    ? JSON.parse(localStorage.getItem(ACCESS_TOKEN))
    : false

  const currentUser = localStorage.getItem(CURRENT_USER) !== null
    ? JSON.parse(localStorage.getItem(CURRENT_USER))
    : undefined

  useEffect(() => {
    setAmountCart(cartItems.length)
  }, [cartItems])

  useEffect(() => {
    setKeyMenu(location.pathname);
  }, [location])

  useEffect(() => {
    // const produc
  },[])

  const onSelectProduct = (value) => {
    const product = productList.find(product => product.productName === value)
    setProduct(product)
    showModal()
  };
  
  const showModal = () => {
    setVisibleModel(true);
  };

  const closeModal = () => {
    setVisibleModel(false);
  }

  const handleVisibleChange = flag => {
    setVisible(flag);
  };

  const handleMenuClick = e => {
    switch (e.key) {
      case '1':
        break;

      case '2':

        break;

      case '3':
        dispatch(authActions.logout())
        break;

      default:
        break;
    }
  };

  const menuUser = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">
        <Link to='/user_info'>
          Thông tin người dùng
        </Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link to='/user_cart'>
          Đơn hàng
        </Link>
      </Menu.Item>
      <Menu.Item key="3">Đăng xuất</Menu.Item>
    </Menu>
  );

  return (
    <div className="header-with-search">
      <Row>
        <Col span={8}>
          <Link to="/">
            <Avatar size="large" src={logo}
              style={{
                width: 120,
                height: 120,
                margin: '-10px 0 0 40px'
              }} />
          </Link>
        </Col>
        <Col span={8}>
          <InputSearch
            productList={productList}
            onShowModal={showModal}
            onSelectProduct={onSelectProduct}
          />
        </Col>
        <Col span={3} push={5}>
          <div style={{ display: 'flex', alignItems: 'flex-end' }}>
            <Link to="/cart" >
              <Badge count={amountCart}>
                <ShoppingCartOutlined
                  style={{
                    fontSize: 30,
                  }} />
              </Badge>
            </Link>
            {isLoggedIn
              ?
              <Dropdown
                arrow
                overlay={menuUser}
                onVisibleChange={handleVisibleChange}
                visible={visible}
                placement="bottomRight"
              >
                <Avatar
                  size={48}
                  icon={<UserOutlined />}
                  style={{ backgroundColor: '#1890ff', marginLeft: 50 }}
                />
              </Dropdown>
              :
              <Link to="/sign_in">
                <Button type='primary' style={{ height: 48, marginLeft: 26 }}>
                  ĐĂNG NHẬP
                </Button>
              </Link>
            }
          </div>
        </Col>
      </Row>

      <div>
        {visibleModel &&
          <Modal
            title="Thông tin sản phẩm"
            centered
            visible={visibleModel}
            onOk={closeModal}
            onCancel={closeModal}
            width={1000}
            footer={null}
          >
            <ProductDetails
              product={product}
              onCloseModal={closeModal} />
          </Modal>}
      </div>
    </div>
  )
}

export default HeaderWithSearch