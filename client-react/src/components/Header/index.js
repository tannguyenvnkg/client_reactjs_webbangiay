import HeaderNavbar from "../HeaderNavbar";
import HeaderWithSearch from "../HeaderWithSearch";
function Header() {
    return (
        <header className="header">
            {/* <div className="grid"> */}
                <HeaderWithSearch/>  {/* thanh tìm kiếm */}
                <HeaderNavbar /> {/* link pages */}
            {/* </div> */}
        </header>
    );
}

export default Header