import { Col, Row } from 'antd'
import FooterAbout from 'components/FooterAbout'
import FooterContact from 'components/FooterContact'
import FooterPages from 'components/FooterPages'
import FooterRecommend from 'components/FooterRecommend'
import React from 'react'

function Footer() {
  return (
    <footer className="footer">
      <Row justify='space-around' style={{ paddingTop: 30}}>
        <Col span={8} offset={3}>
          <div className="footer-item" >
            <FooterAbout />
          </div>
        </Col>
        
        <Col span={8}>
          <div className="footer-item" >
            <FooterContact />
          </div>
        </Col>
      </Row>
    </footer>
  )
}

export default Footer