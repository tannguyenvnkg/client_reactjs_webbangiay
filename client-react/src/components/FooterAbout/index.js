import React from 'react'
import { logoLarge } from 'constants/images'
export default function FooterAbout() {
  return (
    <>
      <div className="footer-item__context"></div>
      <img 
      src={logoLarge} 
      style={{
        width: '75%',
        backgroundSize: 'cover',
        marginTop: '10px'
      }}/>

    </>
  )
}
