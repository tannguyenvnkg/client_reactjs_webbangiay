import createSagaMiddleware from "@redux-saga/core"
import { configureStore, ThunkAction, Action, combineReducers } from "@reduxjs/toolkit"
import { connectRouter, routerMiddleware } from "connected-react-router";
import adminReducer from "features/Admin/adminSlice";
import authReducer from "features/Auth/authSlice";
import cartReducer from "features/Cart/cartSlice";
import homeReducer from "features/Home/homeSlice";
import productMenuReducer from "features/Product/productMenuSlice";
import userReducer from "features/User/userSlice";
import { history } from "utils/history";
import rootSaga from "./rootSaga";

const rootReducer = combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    home: homeReducer,
    productMenu: productMenuReducer,
    cart: cartReducer,
    admin: adminReducer,
    user: userReducer,
})

const sagaMiddleware = createSagaMiddleware()
export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(sagaMiddleware, routerMiddleware(history)),
});

sagaMiddleware.run(rootSaga);

// export const AppDispatch = store.dispatch;
// export const RootState = store.getState;
