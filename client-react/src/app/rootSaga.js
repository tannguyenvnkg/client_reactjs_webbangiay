import adminSaga from "features/Admin/adminSaga";
import authSaga from "features/Auth/authSaga";
import cartSaga from "features/Cart/cartSaga";
import homeSaga from "features/Home/homeSaga";
import productMenuSaga from "features/Product/productMenuSaga";
import userSaga from "features/User/userSaga";
import { all } from "redux-saga/effects";

export default function* rootSaga(){
    yield all([
        authSaga(), 
        homeSaga(), 
        productMenuSaga(), 
        cartSaga(),
        adminSaga(),
        userSaga(),
    ]);
}