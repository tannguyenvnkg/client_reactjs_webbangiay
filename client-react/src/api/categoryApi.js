import axiosClient from "./axiosClient";

// api/categoryApi.js
const CategoryApi = {
    getAll: (params) => {
        const url = '/category/getAllCategory';
        return axiosClient.get(url, params);
    },

    add: (params) => {
        const url = '/category/addCategory'
        return axiosClient.post(url, params);
    },

    update: (params) => {
        const url = '/category/updateCategory';
        return axiosClient.put(url, params)
    }
}
export default CategoryApi