import axiosClient from "./axiosClient";

// api/userApi.js
const UserApi = {
    loginCustomer: (data) => {
        const url = '/customer/loginCustomer';
        return axiosClient.post(url, data);
    },

    registerCustomer: (data) => {
        const url = '/customer/registerCustomer';
        return axiosClient.post(url, data);
    },

    updateCustomer: (data) => {
        const url = '/customer/updateCustomer';
        return axiosClient.put(url, data);
    },

    receiveOTP: (data) => {
        const url = '/customer/receiveOTP';
        return axiosClient.post(url, data);
    },

    checkResetCode: (data) => {
        const url = '/customer/checkResetCode';
        return axiosClient.post(url, data);
    },

    resetPassword: (data) => {
        const url = '/customer/resetPassword';
        return axiosClient.put(url, data);
    },
    
    changePassword: (data) => {
        const url = '/customer/changePassword';
        return axiosClient.put(url, data)
    }
}
export default UserApi