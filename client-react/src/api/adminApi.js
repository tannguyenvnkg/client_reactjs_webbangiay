import axiosClient from "./axiosClient";

// api/adminApi.js
const AdminApi = {
    getAll: () => {
        const url = 'admin/getAllAdmin'
        return axiosClient.get(url);
    },

    getAdminByUsername: (username) => {
        const url = 'admin/getAdminByUsername'
        return axiosClient.get(url, username);
    },

    loginAdmin: (data) =>{
        const url = 'admin/loginAdmin'
        return axiosClient.post(url, data)
    },

    registerAdmin: (data) => {
        const url = 'admin/registerAdmin'
        return axiosClient.post(url, data)
    },
    
}
export default AdminApi