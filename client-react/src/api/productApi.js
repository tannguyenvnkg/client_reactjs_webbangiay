import axiosClient from "./axiosClient";

// api/productApi.js
const ProducerApi = {
    getAll: (params) => {
        const url = '/product/getAllProduct';
        return axiosClient.get(url,  params );
    },

    getByCategoryId: (params) => {
        const url = '/product/getProductByCategoryID';
        return axiosClient.get(url,  {params} );
    },

    getByProviderId: (params) => {
        const url = '/product/getProductByProviderID';
        return axiosClient.get(url, {params} );
    },

    addProduct: (params, config) => {
        const url = '/product/addProduct';
        return axiosClient.post(url, params, config)
    },

    updateProduct: (params) => {
        const url = '/product/updateProduct';
        return axiosClient.put(url, params)
    },

    updateImage: (params, config) => {
        const url = '/product/updateImageProduct';
        return axiosClient.put(url,  params , config)
    },

    addSizeProduct: (data) => {
        const url = '/product/addSizeProduct';
        return axiosClient.post(url, data)
    }

    
}
export default ProducerApi