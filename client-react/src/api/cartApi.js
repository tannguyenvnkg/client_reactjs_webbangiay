import axiosClient from "./axiosClient";

// api/cartApi.js
const CartApi = {
    addOrder: (data) => {
        const url = '/order/addOrder';
        return axiosClient.post(url, data);
    },

    getOrderByEmail: ({ customerEmail }) => {
        const url = `/order/getOrderByCustomerId?customerEmail=${customerEmail}`;
        return axiosClient.get(url)
    },

    getOrderById: ({ orderId }) => {
        const url = `/order/getOrderById?orderId=${orderId}`;
        return axiosClient.get(url)
    },

    getAll: () => {
        const url = '/order/getAllOrder';
        return axiosClient.get(url)
    },

    getOrderBeingDelivered: () => {
        const url = '/order/getOrderBeingDelivered';
        return axiosClient.get(url)
    },

    getOrderDeliverySuccess: () => {
        const url = '/order/getOrderDeliverySuccess';
        return axiosClient.get(url)
    },

    getOrderDeliveryCancel: () => {
        const url = '/order/getOrderDeliveryCancel';
        return axiosClient.get(url)
    },

    deliverySuccess: (orderId) => {
        const url = '/order/deliverySuccess';
        return axiosClient.put(url, orderId)
    },

    deliveryCancel: (orderId) => {
        const url = '/order/deliveryCancel';
        return axiosClient.put(url, orderId)
    }
}
export default CartApi