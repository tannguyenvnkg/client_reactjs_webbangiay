import axiosClient from "./axiosClient";

// api/providerApi.js
const ProviderApi = {
    getAll: () => {
        const url = '/provider/getAllProvider';
        return axiosClient.get(url);
    },

    add: (params) => {
        const url = '/provider/addProvider'
        return axiosClient.post(url, params)
    },
    
    update: (params) => {
        const url = '/provider/updateProvider';
        return axiosClient.put(url, params)
    }

}
export default ProviderApi