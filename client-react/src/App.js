import Loading from 'components/Loading';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const NotFound = React.lazy(() => import('./pages/NotFound'))
const DashboardAdmin = React.lazy(() => import('./pages/DashboardAdmin'))
const Dashboard = React.lazy(() => import('./pages/Dashboard'))

function App() {

  return (
    <div>
      <React.Suspense fallback={<Loading/>}>
        <Switch>
          {/* ADMIN */}
          <Route path="/a_m_p_main">
            <DashboardAdmin />
          </Route>


          {/* USER  */}
          <Route path="/">
            <Dashboard />
          </Route>
          <Route exact path="*"><NotFound /></Route>
          {/* <Footer /> */}
        </Switch>
      </React.Suspense>
    </div>
  );
}

export default App;
