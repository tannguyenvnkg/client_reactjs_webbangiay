import { ACCESS_TOKEN_ADMIN } from 'constants/global'
import React from 'react'
import { Redirect } from 'react-router-dom'
import { Route } from 'react-router-dom'

export default function PrivateRoute({ children, ...props}) {
    const isLoggedIn = Boolean(localStorage.getItem(ACCESS_TOKEN_ADMIN))

    if (!isLoggedIn) return <Redirect to="/a_m_p_main/sign_in"/>;
    
    return (
        <Route {...props}>{children}</Route>
    )
}
