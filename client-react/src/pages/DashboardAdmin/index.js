// import Admin from 'features/Admin/pages/Admin';
import Loading from 'components/Loading';
import SignInAdmin from 'features/Auth/pages/SignInAdmin';
import { Admin } from 'pages';
import NotFound from 'pages/NotFound';
import PrivateRoute from 'pages/PrivateRoute';
import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

export default function DashboardAdmin() {
    const { path } = useRouteMatch();
    return (
        <>
            <React.Suspense fallback={<Loading />}>
                <Switch>
                    {/* <Content /> */}
                    <Route path={`${path}/sign_in`}><SignInAdmin /></Route>
                    <PrivateRoute path={`${path}/home`}><Admin /></PrivateRoute>

                    <Redirect from={`${path}`} to={`${path}/home`} />
                    <Route path="*"><NotFound /></Route>
                </Switch>
            </React.Suspense>
        </>
    )
}
