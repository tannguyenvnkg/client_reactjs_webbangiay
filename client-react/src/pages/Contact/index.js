import React from 'react'
import { Card, Col, Row } from 'antd';
import { avatar } from 'constants/images'
function Contact() {
  const { Meta } = Card;
  return (
    <div className="site-card-wrapper">
      <Row justify="center" gutter={16}>
        <Col span={14}><h1 className="home-item__title" style={{ textAlign: 'center', marginBottom: 30 }}>THÀNH VIÊN DỰ ÁN</h1></Col>
        <Col span={14}>
          <Row justify="space-around" style={{ marginBottom: 30}}>
            <Card
              hoverable
              cover={<img style={{
                width: 200,
                height: 220,
                textAlign: 'center',
              }}
                alt="avatar" src={avatar.tan} />}
            >
              <Meta title="Nguyễn Phước Tấn"/>
            </Card>

            <Card
              hoverable
              cover={<img style={{
                width: 200,
                height: 220,
                textAlign: 'center',
              }}
                alt="avatar" src={avatar.si} />}
            >
              <Meta title="Phan Quốc Sĩ"/>
            </Card>

            <Card
              hoverable
              cover={<img style={{
                width: 200,
                height: 220,
                textAlign: 'center',
              }}
                alt="avatar" src={avatar.nghia} />}
            >
              <Meta title="Nguyễn Trọng Nghĩa"/>
            </Card>
          </Row>
        </Col>

        <Col span={14}>
          <Row justify="space-around">
            <Card
              hoverable
              cover={<img style={{
                width: 200,
                height: 220,
                textAlign: 'center',
              }}
                alt="avatar" src={avatar.phong} />}
            >
              <Meta title="Dương Thanh Phong"/>
            </Card>

            <Card
              hoverable
              cover={<img style={{
                width: 200,
                height: 220,
                textAlign: 'center',
              }}
                alt="avatar" src={avatar.hai} />}
            >
              <Meta title="Trần Ngọc Hải"/>
            </Card>
          </Row>
        </Col>
      </Row>


    </div>
  )
}

export default Contact