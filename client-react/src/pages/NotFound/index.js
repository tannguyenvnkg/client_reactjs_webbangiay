import { Button } from 'antd'
import React from 'react'

function NotFound() {
  return (
    <div className="notfound">
      <h1 className="notfound__header">Oops!!!</h1>
      <h3 className="notfound__title">Không tìm thấy trang...</h3>
      <Button
        type="primary"
        shape="round"
        size="large"
      >
        Trở lại trang chủ
      </Button>
    </div>
  )
}

export default NotFound