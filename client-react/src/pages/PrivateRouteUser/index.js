import { ACCESS_TOKEN } from 'constants/global'
import React from 'react'
import { Redirect } from 'react-router-dom'
import { Route } from 'react-router-dom'

export default function PrivateRouteUser({ children, ...props}) {
    const isLoggedIn = Boolean(localStorage.getItem(ACCESS_TOKEN))

    if (!isLoggedIn) return <Redirect to="/sign_in"/>;
    
    return (
        <Route {...props}>{children}</Route>
    )
}
