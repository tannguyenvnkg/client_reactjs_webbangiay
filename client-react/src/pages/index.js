export {default as Home} from '../features/Home/pages/Home'
export {default as Contact} from './Contact'
export {default as Admin} from '../features/Admin/pages/Admin'
export {default as SignIn} from '../features/Auth/pages/SignIn'
export {default as PrivateRoute} from './PrivateRoute'
