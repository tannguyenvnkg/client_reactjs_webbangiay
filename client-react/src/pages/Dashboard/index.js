import Loading from 'components/Loading';
import PrivateRouteUser from 'pages/PrivateRouteUser';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Footer, Header } from '../../components';
const SignIn = React.lazy(() => import('features/Auth/pages/SignIn'))
const ProductMenu = React.lazy(() => import('features/Product/pages/ProductMenu'))
const ProductDetails = React.lazy(() => import('features/Product/pages/ProductDetails'))
const Cart = React.lazy(() => import('features/Cart/pages/Cart'))
const NotFound = React.lazy(() => import('../NotFound'))
const Home = React.lazy(() => import('features/Home/pages/Home'))
const Contact = React.lazy(() => import('../Contact'))
const Register = React.lazy(() => import('features/Auth/pages/Register'))
const UserInfo = React.lazy(() => import('features/User/pages/UserInfo'))
const UserCart = React.lazy(() => import('features/User/pages/UserCart'))
const ForgotPassword = React.lazy(() => import('features/Auth/pages/ForgotPassword'))

export default function Dashboard() {
    return (
        <>
            <Header />
            <div className="content-app">
                <React.Suspense fallback={<Loading />}>
                    <Switch>
                        {/* <Content /> */}
                        <Route path={`/home`}><Home /></Route>
                        <Route path={`/contact`}><Contact /></Route>
                        <Route path={`/sign_in`}><SignIn /></Route>
                        <Route path={`/register`}><Register /></Route>
                        <PrivateRouteUser path={`/user_info`}><UserInfo /></PrivateRouteUser>
                        <PrivateRouteUser path={`/user_cart`}><UserCart /></PrivateRouteUser>
                        <Route path={`/cart`}><Cart /></Route>
                        <Route path={`/forgot_password`}><ForgotPassword /></Route>
                        <Route path={`/product_menu`}><ProductMenu /></Route>
                        <Route path={`/product_details/:productDetailsId`}><ProductDetails /></Route>
                        <Redirect exact from="/" to="/home" />
                        <Route path="*"><NotFound /></Route>
                    </Switch>
                </React.Suspense>
            </div>

            <Footer />
        </>
    )
}
