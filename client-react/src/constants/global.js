export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';


// ACCESS_TOKEN
export const ACCESS_TOKEN = 'access_token';
// ACCESS_TOKEN
export const ACCESS_TOKEN_ADMIN = 'access_token_admin';
// CART
export const CART = 'cart_items';
// CURRENT_USER 
export const CURRENT_USER = 'current_user';
// EMAIL
export const EMAIL = 'email';
// RESET_CODE
export const RESET_CODE = 'reset_code';


// CURRENT_ADMIN 
export const CURRENT_ADMIN = 'current_admin';



export const mess = {
    customer : {
        name: 'Vui lòng nhập tên khách hàng!',
        phone: 'Vui lòng nhập số điện thoại!',
        address: 'Vui lòng nhập địa chỉ!',
        email: 'Vui lòng nhập email!',
        password: 'Vui lòng nhập mật khẩu!',
        oldPassword: 'Vui lòng nhập mật khẩu hiện tại!',
        passwordConfirm : 'Vui lòng nhập xác nhận mật khẩu!',
        birthday: 'Vui lòng chọn ngày sinh',
    },
    product: {
        productName: 'Vui lòng nhập tên sản phẩm!',
        price: 'Vui lòng nhập giá!',
        category: 'Vui lòng chọn thể loại!',
        provider: 'Vui lòng chọn nhà cung cấp!',
        size: 'Vui lòng nhập size!',
        quantity: 'Vui lòng nhập số lượng!',
    },
    category: {
        categoryName: 'Vui lòng nhập tên thể loại!',
    },
    provider: {
        productName: 'Vui lòng nhập tên nhà cung cấp!',
        address: 'Vui lòng nhập địa chỉ!',
        phone: 'Vui lòng nhập số điện thoại',
    },
}

export const placeholder = {
    customer : {
        name: 'Nhập tên khách hàng...',
        phone: 'Nhập số điện thoại...',
        address: 'Nhập địa chỉ...',
        email: 'Nhập email...',
        password: 'Nhập mật khẩu...',
        passwordConfirm : 'Nhập xác nhận mật khẩu...',
        birthday: 'Chọn ngày sinh...',
    },
}