
import banner1 from '../assets/img/banner-adidas-ngang-1.jpg'
import banner2 from '../assets/img/banner-nike-ngang-1.jpg'
import banner3 from '../assets/img/banner-vans-ngang-1.jpg'

import banner4 from '../assets/img/banner-doc-1.jpg'
import banner5 from '../assets/img/banner-doc-2.jpg'
import logoApp from '../assets/img/logo.png'
import logoLargeApp from '../assets/img/logo-large.png'
import emptyCartImg from '../assets/img/gio-hang-trong.jpg'

import avatarTan from '../assets/img/avatarTan.jpg'
import avatarSi from '../assets/img/avatarSi.jpg'
import avatarNghia from '../assets/img/avatarNghia.jpg'
import avatarHai from '../assets/img/avatarHai.jpg'
import avatarPhong from '../assets/img/avatarPhong.jpg'

const Banner = {
    banner1: banner1,
    banner2: banner2,
    banner3: banner3,
    banner4: banner4,
    banner5: banner5,
}

export const avatar = {
    nghia: avatarNghia,
    tan: avatarTan,
    si: avatarSi,
    hai: avatarHai,
    phong: avatarPhong,
}

export const emptyCart = emptyCartImg
export const logo = logoApp
export const logoLarge = logoLargeApp

export default Banner;