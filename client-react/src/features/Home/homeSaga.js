import { takeLatest, all, call, put } from 'redux-saga/effects'
import { homeActions } from './homeSlice'
import ProductApi from 'api/productApi'
import CategoryApi from 'api/categoryApi';
import ProviderApi from 'api/providerApi';

function* fetchProductList() {
    const { data } = yield call(ProductApi.getAll);

    yield put(homeActions.setProductList(data));
}

function* fetchCategoryList() {
    const { data } = yield call(CategoryApi.getAll);

    yield put(homeActions.setCategoryList(data));
}

function* fetchProviderList() {
    const { data } = yield call(ProviderApi.getAll);

    yield put(homeActions.setProviderList(data));
}

function* fetchHomeData() {
    try {
        yield all([
            call(fetchProductList),
            call(fetchCategoryList),
            call(fetchProviderList),
        ])
        yield put(homeActions.fetchDataSuccess());
    } catch (error) {
        console.log("failed to fetch home data", error);
        yield put(homeActions.fetchDataFailed());
    }
}

export default function* homeSaga() {
    yield takeLatest(homeActions.fetchData.type, fetchHomeData);
}