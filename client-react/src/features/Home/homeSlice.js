const { createSlice } = require("@reduxjs/toolkit");

const homeState = {
    loading: false,
    productList: [],
    categoryList: [],
    providerList: []
}

const homeSlice = createSlice({
    name: "home",
    initialState: homeState,
    reducers: {
        fetchData(state) {
            state.loading = true;
        },
        fetchDataSuccess(state) {
            state.loading = false;
        },
        fetchDataFailed(state) {
            state.loading = false;
        },

        setProductList(state, action) {
            state.productList = action.payload
        },
        setCategoryList(state, action) {
            state.categoryList = action.payload
        },
        setProviderList(state, action) {
            state.providerList = action.payload
        },
    },
});

// Actions
export const homeActions = homeSlice.actions;

// Selectors
export const selectLoading = (state) => state.home.loading
export const selectCategoryList = (state) => state.home.categoryList
export const selectProviderList = (state) => state.home.providerList
export const selectProductList = (state) => state.home.productList

// Reducer
const homeReducer = homeSlice.reducer;
export default homeReducer;