import React from 'react'

function BannerItem(props) {
    return (
        <div className="banner-item">
            <img src={props.img} />
        </div>
    )
}

export default BannerItem