import { Carousel } from 'antd';
import Banner from 'constants/images';
import React from 'react';

function SlideBar() {
  const arrBanner = [Banner.banner1, Banner.banner2, Banner.banner3];

  return (
    <div className="banner-slider">
      <Carousel
        autoplay>
        {arrBanner.map((item, index) => {
          return (
            <img src={item} key={index} />
          )
        })}
      </Carousel>
    </div>
  );
}

export default SlideBar