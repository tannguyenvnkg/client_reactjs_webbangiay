import { Col, Row } from 'antd'
import { Categories, Providers } from 'components'
import Banner from 'constants/images'
import { BannerItem, SlideBar } from 'features/Home'
import {
  homeActions,
  selectCategoryList, selectProductList,
  selectProviderList
} from 'features/Home/homeSlice'
import Products from 'features/Product/components/Products'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

function Home() {

  const dispatch = useDispatch()
  const categoryList = useSelector(selectCategoryList)
  const providerList = useSelector(selectProviderList)
  const productList = useSelector(selectProductList)

  useEffect(() => {
    dispatch(homeActions.fetchData());
  }, [dispatch])

  return (
    <>
      {/* container */}
      <div className="home-container">
        {/* banner */}
        <SlideBar />
      </div>

      {/* content */}
      <div className="home-container">
        <Row
          gutter={16}
          justify="center"
          style={{
            marginLeft: 0,
            marginRight: 0
          }}>
          <Col span={6}>
            <BannerItem img={Banner.banner4} />
          </Col>

          <Col span={16}>
            <Row>
              {/* provider */}
              <Col flex={1}>
                <div className="home-item">
                  <Providers providerList={providerList} />
                </div>
              </Col>

              {/* category */}
              <Col flex={1}>
                <div className="home-item">
                  <Categories categoryList={categoryList} />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>

      {/* product */}
      <div className="home-container">
        <Row
          gutter={16}
          justify="center"
          style={{
            marginLeft: 0,
            marginRight: 0
          }}>
          <Col span={16}>
            <div className="home-item">
              <Products productList={productList} />
            </div>
          </Col>
          <Col span={6}>
            <BannerItem img={Banner.banner5} />
          </Col>
        </Row>
      </div>
    </>
  )
}

export default Home