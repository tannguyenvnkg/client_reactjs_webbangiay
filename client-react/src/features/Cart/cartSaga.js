import { takeLatest, call, take, put, fork } from 'redux-saga/effects'
import { cartActions } from './cartSlice'
import ProductApi from 'api/productApi'
import CartApi from 'api/cartApi';
import { CART } from 'constants/global';
import { message } from 'antd';


function* fetchProductList() {
    try {
        const { data } = yield call(ProductApi.getAll);

        yield put(cartActions.setProductList(data));
        yield put(cartActions.fetchDataSuccess());
    } catch (error) {
        yield put(cartActions.fetchDataFailed());
    }

}

function* handleAddToCart(payload) {
    yield put(cartActions.addToCartSuccess(payload));
}

function* handleUpdateAmount(payload) {
    yield put(cartActions.updateAmountSuccess(payload));
}

function* handleRemoveItem(payload) {
    yield put(cartActions.removeItemSuccess(payload));
}

function* handleOrder(payload) {
    const data = yield call(CartApi.addOrder, payload);
    if (data.error) {
        console.log('order failed', data.message);
        message.error(data.message);
        yield put(cartActions.orderFailed(data));
    } else {
        console.log('order success', data);
        message.success(data.message)
        yield put(cartActions.orderSuccess());
    }
}

function* watchAddToCardFlow() {
    while (true) {
        const action = yield take(cartActions.addToCart.type);
        yield fork(handleAddToCart, action.payload);
    }
}

function* watchUpdateAmountFlow() {
    while (true) {
        const action = yield take(cartActions.updateAmount.type);
        yield fork(handleUpdateAmount, action.payload);
    }
}

function* watchRemoveItemFlow() {
    while (true) {
        const action = yield take(cartActions.removeItem.type);
        yield fork(handleRemoveItem, action.payload);
    }
}

function* watchOrderFlow() {
    while (true) {
        const action = yield take(cartActions.order.type);
        yield fork(handleOrder, action.payload);
    }
}

export default function* cartSaga() {
    yield fork(watchAddToCardFlow);
    yield fork(watchUpdateAmountFlow);
    yield fork(watchRemoveItemFlow);
    yield fork(watchOrderFlow);
    yield takeLatest(cartActions.fetchData.type, fetchProductList);
}