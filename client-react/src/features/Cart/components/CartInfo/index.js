import { Descriptions, Form, Input } from 'antd'
import { AreaField, InputField } from 'components/FormFields';
import React, { memo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';

function CartInfo({ initialValues, onSubmit, form, isLoggedIn }, props) {
    const {
        control,
        handleSubmit,
    } = useForm({
        defaultValues: initialValues,
    })

    // console.log('isLoggedIn', isLoggedIn);
    const [user, setUser] = useState({});

    useEffect(() => {
        if (initialValues) {
            setUser(initialValues)
        }
        // console.log('user', user);
    }, [initialValues])

    const { TextArea } = Input;

    const handleCartFormSubmit = async (formValues) => {
        try {
            await onSubmit?.(formValues);
        } catch (e) {
            console.log('Failed : ', e);
        }
    }

    return (
        <div style={{ padding: '20px' }}>

            <Form
                form={form}
                onFinish={handleSubmit(handleCartFormSubmit)}
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}>
                <Form.Item // customer Name
                    label="Tên khách hàng"
                    rules={[{ required: true, message: 'Vui lòng nhập tên khách hàng!' }]}
                >
                    <InputField
                        name="customerName"
                        control={control}
                        placeholder='Nhập tên khách hàng...' />
                </Form.Item>

                <Form.Item // phone
                    label="Số điện thoại"
                    rules={[{ required: true, message: 'Vui lòng nhập số điện thoại!' }]}
                >
                    <InputField
                        name="phone"
                        control={control}
                        placeholder='Nhập số điện thoại...'
                    />
                </Form.Item>

                <Form.Item // email
                    label="email"
                    rules={[{ required: true, message: 'Vui lòng nhập email!' }]}
                >
                    <InputField
                        disabled={isLoggedIn}
                        name="email"
                        control={control}
                        type="email"
                        placeholder="Nhập email..."
                    />
                </Form.Item>

                <Form.Item // address
                    label="Địa chỉ nhận hàng"
                    rules={[{ required: true, message: 'Vui lòng nhập địa chỉ nhận hàng!' }]}
                >
                    <AreaField
                        name="address"
                        control={control}
                        placeholder="Nhập địa chỉ nhận hàng..."
                        autoSize={{ minRows: 3, maxRows: 5 }}
                    />
                </Form.Item>
            </Form>
        </div>
    )
}

export default memo(CartInfo)