import { Button } from 'antd'
import { emptyCart } from 'constants/images'
import React from 'react'
import { useHistory } from 'react-router-dom'

function EmptyCart() {

    const history = useHistory()

    const handleGoToProductList = () => {
        history.push('/product_menu')
    }
    return (
        <div className="empty-cart__container">
            <img className="empty-cart__item empty-cart__img" src={emptyCart} width={150} />
            <div className="empty-cart__item empty-cart__title"> Giỏ hàng của bạn đang trống.</div>
            <div className="empty-cart__item empty-cart__btn">
                <Button
                    type="primary"
                    onClick={() => handleGoToProductList()}
                    style={{
                        padding: '8px 20px',
                        height: 'auto'
                    }}>
                    <div className="btn-title">Đến cửa hàng</div>
                </Button>
            </div>
        </div>
    )
}

export default EmptyCart