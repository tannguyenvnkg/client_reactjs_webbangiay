import { ShoppingCartOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React, { memo } from 'react';
import { useHistory } from 'react-router-dom';

function CartOrder(props) {
    const history = useHistory()

    const totalPrice = props.totalPrice;
    const totalProducts = props.totalProducts;
    const handleCartFormSubmit = props.handleCartFormSubmit
    const form = props.form

    const handleGoToProductList = () => {
        history.push('/product_menu')
    }

    return (
        <div className="cart-order">
            <div className="cart-order__price">
                <div className="title">Tổng thanh toán ( {totalProducts} sản phẩm ) : </div>
                <div className="price">{totalPrice}</div>
            </div>
            <Button
                type="primary"
                icon={<ShoppingCartOutlined className="submit-icon" />}
                size="large"
                onClick={() => handleGoToProductList()}
                style={{
                    padding: '8px 20px',
                    height: 'auto',
                }}
            >
                Tiếp tục mua hàng
            </Button>
            <Button
                htmlType='submit'
                type="primary"
                icon={<ShoppingCartOutlined className="submit-icon" />}
                onClick={() => form.submit()}
                size="large"
                style={{
                    padding: '8px 20px',
                    height: 'auto'
                }}
            >
                Đặt hàng
            </Button>
        </div>
    )
}

export default memo(CartOrder)