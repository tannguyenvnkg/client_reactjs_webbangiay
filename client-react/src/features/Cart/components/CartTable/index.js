import { Checkbox } from 'antd';
import DetailItem from 'features/User/components/DetailItem';
import React from 'react';
import CartItem from '../CartItem';

function CartTable(props) {
    const cartProduct = props.cartProduct
    const cartOrderList = props.cartOrderList

    
    return (
        <div className="cart-table__container">
            <table className="cart-table">
                <thead >
                    <tr className="cart-table__row">
                        <th className="cart-table__item item__title">Sản phẩm</th>
                        <th className="cart-table__item item__price">Đơn giá</th>
                        <th className="cart-table__item item__amount">Số lượng</th>
                        <th className="cart-table__item item__total">Thành tiền</th>
                        {cartOrderList ? null : <th className="cart-table__item item__action">Thao tác</th>}
                    </tr>
                </thead>
                <tbody>
                    {cartProduct && cartProduct.map((item, index) => (
                        <CartItem cartItem={item} key={index} />
                    ))}
                    {cartOrderList && cartOrderList.map((item, index) => (
                        <DetailItem cartItem={item} key={index} />
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default CartTable