import { Button, Checkbox, Image, InputNumber, Modal } from 'antd'
import { cartActions } from 'features/Cart/cartSlice'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { formatCash } from 'utils'
import { WarningFilled } from '@ant-design/icons';

function CartItem(props) {
    const { confirm } = Modal;
    const dispatch = useDispatch()

    const [item, setItem] = useState(props.cartItem)
    const [amount, setAmount] = useState(props.cartItem.amount)
    const [quantityInStock, setQuantityInStock] = useState(props.cartItem.quantityInStock)

    useEffect(() => {
        setItem(props.cartItem)
        setAmount(props.cartItem.amount)
        setQuantityInStock(props.cartItem.quantityInStock)
    }, [props.cartItem])

    const totalPrice = item.price * item.amount

    const handleAmount = (value) => {
        (value > quantityInStock || value < 0)
            ? console.log('Số vượt quá')
            : dispatch(cartActions.updateAmount({
                ...item,
                amount: value,
            }))
    }

    const showDeleteConfirm =()=> {
        confirm({
          title: 'Xóa sản phẩm?',
          icon: <WarningFilled style={{color: '#ff4d4f'}}/>,
          content: `Bạn muốn xóa sản phẩm \"${item.product?.productName}\" khỏi giỏ hàng?`,
          okText: 'Xóa',
          okType: 'danger',
          cancelText: 'Hủy',
          onOk() {
            dispatch(cartActions.removeItem(item));
          },
        });
      }
    return (
        <tr className="cart-table__row">
            <td className="cart-table__item item__info">
                <div className="item__info-image">
                    <Image
                        width={110}
                        src={item.product?.imageProduct}
                    />
                </div>
                <div className="item__info-title">
                    {`${item.product?.productName} ( size ${item.size} )`}
                </div>

            </td>
            <td className="cart-table__item item__price">
                {formatCash(item.price)}
            </td>
            <td className="cart-table__item item__amount">
                <InputNumber
                    min={1}
                    max={quantityInStock}
                    // defaultValue={0}
                    value={amount}
                    onChange={(e) => handleAmount(e)}
                >
                </InputNumber>
            </td>
            <td className="cart-table__item item__total">
                {formatCash(totalPrice)}
            </td>
            <td className="cart-table__item item__action">
                <Button 
                type="primary" 
                danger
                onClick={() => showDeleteConfirm()}>
                    Xóa
                </Button>
            </td>
        </tr>
    )
}

export default CartItem