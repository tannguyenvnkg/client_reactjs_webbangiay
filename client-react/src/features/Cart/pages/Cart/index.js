import { Col, Form, message, Row } from 'antd'
import { ACCESS_TOKEN, CURRENT_USER } from 'constants/global'
import AccessLogin from 'features/Auth/components/AccessLogin'
import { cartActions, selectCartItems, selectProductList } from 'features/Cart/cartSlice'
import CartInfo from 'features/Cart/components/CartInfo'
import CartOrder from 'features/Cart/components/CartOrder'
import CartTable from 'features/Cart/components/CartTable'
import EmptyCart from 'features/Cart/components/EmptyCart'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { formatCash } from 'utils'
import '../../Cart.scss'

function CartDetails() {
    const [form] = Form.useForm();
    const dispatch = useDispatch()
    const productList = useSelector(selectProductList)
    const cartItems = useSelector(selectCartItems)

    const [cartProduct, setCartProduct] = useState([]);
    const [totalProducts, setTotalProducts] = useState(0);
    const [totalPrice, setTotalPrice] = useState(0);

    const [currentUser, setCurrentUser] = useState(undefined)
    const [isLoggedIn, setIsLoggedIn] = useState(undefined);
    const [deliveryAddress, setDeliveryAddress] = useState('')

    useEffect(() => {
        dispatch(cartActions.fetchData());
    }, [dispatch])

    // User
    useEffect(() => {
        const user = localStorage.getItem(CURRENT_USER) !== null
            ? JSON.parse(localStorage.getItem(CURRENT_USER))
            : [];
        setCurrentUser(user);
    }, []);

    // Check login
    useEffect(() => {
        const loggedIn = localStorage.getItem(ACCESS_TOKEN) !== null
            ? JSON.parse(localStorage.getItem(ACCESS_TOKEN))
            : false;
        setIsLoggedIn(loggedIn);
    }, [])

    // lọc sản phẩm theo id
    const getItemById = (_id) => productList.find(p => p._id === _id)

    // lấy danh sách sản phẩm giỏ hàng
    const getCartItems = (cartItems) => {
        if (cartItems.length) {
            return cartItems.map((item) => ({
                ...item,
                product: getItemById(item._id)
            }))
        }
        return []
    }

    useEffect(() => {
        setCartProduct(getCartItems(cartItems));
        setTotalProducts(cartItems.reduce((total, item) =>
            total + Number(item.amount), 0
        ));
        setTotalPrice(cartItems.reduce((total, item) =>
            total + (Number(item.amount) * Number(item.price)), 0
        ));
    }, [cartItems, productList])


    // const handleChangeValue = (value) => {
    //     setDeliveryAddress(value.target.value);
    // }

    const handleCartFormSubmit = async (formValues) => {
        if (cartProduct.length === 0) {
            message.warning("Danh sách trống");
        } else if (
            Boolean(!formValues.customerName) ||
            Boolean(!formValues.email) ||
            Boolean(!formValues.phone) ||
            Boolean(!formValues.address)
        ) {
            message.warning("Vui lòng nhập đầy đủ thông tin");
        } else {
            const cartOrder = cartProduct.map(item => ({
                sizeId: item.sizeId,
                amount: item.amount,
                productId: item._id,
            }))

            const api = {
                nameCustomer: formValues.customerName,
                emailCustomer: formValues.email,
                phoneCustomer: formValues.phone,
                addressCustomer: formValues.address,
                cart: cartOrder,
            }

            dispatch(cartActions.order(api))
        }
    }

    const initialValues = {
        customerName: '',
        email: '',
        phone: '',
        address: '',
        ...currentUser
    }

    return (
        <>
            <div className="cart-container">
                <Row justify="center">
                    <Col span={18}>
                        {cartItems.length > 0
                            ? <CartTable cartProduct={cartProduct} />
                            : <EmptyCart />}
                    </Col>
                    <Col span={9}>
                        {(Boolean(currentUser) || Boolean(isLoggedIn)) && (
                            <CartInfo
                                isLoggedIn={isLoggedIn}
                                form={form}
                                initialValues={initialValues}
                                onSubmit={handleCartFormSubmit}
                            // deliveryAddress={deliveryAddress} 
                            />
                        )}

                    </Col>
                    <Col span={9}>
                        <CartOrder
                            form={form}
                            totalProducts={totalProducts}
                            totalPrice={formatCash(totalPrice)}
                            handleCartFormSubmit={handleCartFormSubmit}
                        />
                    </Col>
                </Row>
            </div>
        </>
    )
}

export default CartDetails