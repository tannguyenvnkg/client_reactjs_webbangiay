import { CART } from "constants/global";

const { createSlice } = require("@reduxjs/toolkit");

// get items in localStorage
const items = localStorage.getItem(CART) !== null
    ? JSON.parse(localStorage.getItem(CART))
    : []

const cartState = {
    loading: false,
    value: items,
    productList: []
};

const cartSlice = createSlice({
    name: "cart",
    initialState: cartState,
    reducers: {

        fetchData(state) {
            state.loading = true;
        },
        fetchDataSuccess(state) {
            state.loading = false;
        },
        fetchDataFailed(state) {
            state.loading = false;
        },
        setProductList(state, action) {
            state.productList = action.payload
        },

        // Add cart
        addToCart(state, action) {
            state.loading = true;
        },

        addToCartSuccess(state, action) {
            const newItem = action.payload

            const duplicate = findItem(state.value, newItem);

            if (duplicate.length > 0) {
                state.value = duplicateItem(state.value, newItem)
                if (newItem.amount + duplicate[0].amount <= duplicate[0].quantityInStock) {

                    state.value = [...state.value, {
                        ...newItem,
                        id: duplicate[0].id,
                        amount: newItem.amount + duplicate[0].amount
                    }]

                } else {
                    state.value = [...state.value, {
                        ...newItem,
                        id: duplicate[0].id,
                        amount: duplicate[0].amount
                    }]

                    console.log("vượt quá");
                }
            } else {
                state.value = [...state.value, {
                    ...newItem,
                    // nếu danh sách sản phẩm > 0 , thì id của sản phẩm = length + 1, không thì là sản phẩm đầu tiên.
                    id: state.value.length > 0 ? state.value[state.value.length - 1].id + 1 : 1
                }]
            }
            localStorage.setItem(CART, JSON.stringify(sortItem(state.value)))
            state.loading = false
        },

        addToCartFailed(state, action) {
            state.loading = false;
        },

        // Update cart
        updateAmount(state, amount) {
            state.loading = true;
        },

        updateAmountSuccess(state, action) {
            const itemUpdate = action.payload;
            const item = findItem(state.value, itemUpdate)

            if (item.length > 0) {
                state.value = duplicateItem(state.value, itemUpdate);
                state.value = [...state.value, {
                    ...itemUpdate,
                    id: item[0].id,
                }]
                localStorage.setItem(CART, JSON.stringify(sortItem(state.value)))
            }
        },

        updateAmountFailed(state, amount) {
            state.loading = false;
        },


        // Remove cart
        removeItem(state, item) {
            state.loading = true;
        },

        removeItemSuccess(state, action) {
            const item = action.payload;
            state.value = duplicateItem(state.value, item);
            localStorage.setItem(CART, JSON.stringify(sortItem(state.value)))
        },

        removeItemFailed(state, item) {
            state.loading = false;
        },


        order(state, action) {
            state.logging = true;
        },
        orderSuccess(state, action) {
            state.logging = false;
            state.value = [];
            localStorage.setItem(CART, JSON.stringify(state.value))
        },
        orderFailed(state, action) {
            state.logging = false;
        },
    }
})

// lọc sản phẩm có cùng id và cùng size
const findItem = (arr, item) => arr.filter(e => e._id === item._id && e.size === item.size)
const duplicateItem = (arr, item) => arr.filter(e => e._id !== item._id || e.size !== item.size)
const sortItem = (arr) => arr.sort((a, b) => a.id > b.id ? 1 : (a.id < b.id ? -1 : 0))

// Actions
export const cartActions = cartSlice.actions;

// Selectors
export const selectLoading = (state) => state.cart.loading
export const selectCartItems = (state) => state.cart.value
export const selectProductList = (state) => state.cart.productList

// Reducers
const cartReducer = cartSlice.reducer;
export default cartReducer;