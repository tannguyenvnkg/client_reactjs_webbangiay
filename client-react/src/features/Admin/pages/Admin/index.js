import { ShoppingCartOutlined, UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import { Avatar, Dropdown, Layout, Menu } from 'antd';
import { adminActions, selectCategoryList, selectLoading, selectOrderBeingDelivered, selectOrderDeliveryCancel, selectOrderDeliverySuccess, selectOrderList, selectProductList, selectProviderList } from 'features/Admin/adminSlice';
import CategoryAddEdit from 'features/Admin/components/CategoryAddEdit';
import CategoryList from 'features/Admin/components/CategoryList';
import OrderDetails from 'features/Admin/components/OrderDetails';
import OrderList from 'features/Admin/components/OrderList';
import ProductAddEdit from 'features/Admin/components/ProductAddEdit';
import ProductList from 'features/Admin/components/ProductList';
import ProviderAddEdit from 'features/Admin/components/ProviderAddEdit';
import ProviderList from 'features/Admin/components/ProviderList';
import { authActions } from 'features/Auth/authSlice';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Route, Switch, useHistory, useLocation, useRouteMatch } from 'react-router-dom';
import '../../Admin.scss';

const { Header, Content, Footer, Sider } = Layout;

function Admin() {
  const dispatch = useDispatch()
  const history = useHistory()
  let { path, url } = useRouteMatch();
  let location = useLocation();

  const loading = useSelector(selectLoading)
  const categoryList = useSelector(selectCategoryList)
  const providerList = useSelector(selectProviderList)
  const productList = useSelector(selectProductList)
  const orderList = useSelector(selectOrderList)
  const orderBeingDelivered = useSelector(selectOrderBeingDelivered)
  const orderOrderDeliverySuccess = useSelector(selectOrderDeliverySuccess)
  const orderOrderDeliveryCancel = useSelector(selectOrderDeliveryCancel)

  const [keyMenu, setKeyMenu] = useState('');
  const [render, setRender] = useState(false);
  const [visible, setVisible] = useState();

  useEffect(() => {
    setKeyMenu(location.pathname);
  }, [location]);

  useEffect(() => {
    dispatch(adminActions.fetchData());
    setRender(false)
  }, [render]);


  // handle 
  const handleEditProvider = async (providerId) => {
    history.push(`/a_m_p_main/home/provider/${providerId}`)
  }

  const handleEditCategory = async (categoryId) => {
    history.push(`/a_m_p_main/home/category/${categoryId}`)
  }

  const handleEditProduct = async (productId) => {
    history.push(`/a_m_p_main/home/product/${productId}`)
  }

  const handleShowDetails = async (orderId) => {
    history.push(`/a_m_p_main/home/order/${orderId}`)
  }

  const handleEditStatus = (productId) => {
    try {
      const item = productList.find(item => item._id === productId)
      console.log(item);
      dispatch(adminActions.updateProduct({
        _id: item._id,
        productName: item.productName,
        price: item.price,
        categoryId: item.category._id,
        providerId: item.provider._id,
        status: !item.status,
        details: item.details,
      }))
      handleReRender()
    } catch (err) {
      console.log('Failed', err);
    }
  }

  const handleDeliverySuccess = async (orderId) => {
    dispatch(adminActions.updateDeliverySuccess({
      orderId: orderId,
    }))
    await handleReRender()
  }

  const handleDeliveryCancel = async (orderId) => {
    dispatch(adminActions.updateDeliveryCancel({
      orderId: orderId,
    }))
    await handleReRender()
  }

  const handleReRender = () => setRender(true);

  const handleVisibleChange = flag => {
    setVisible(flag);
  };

  const handleMenuClick = e => {
    if (e.key === '2') {
      dispatch(authActions.logoutAdmin())
    }
  };

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">Tên người dùng</Menu.Item>
      <Menu.Item key="2">Đăng xuất</Menu.Item>
    </Menu>
  );

  return (

    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
      >
        <div className="logo" style={{
          height: 32,
          margin: 16,
          background: 'rgba(255, 255, 255, 0.2)',
        }} />
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={keyMenu}
          defaultSelectedKeys={`${path}/product/list`}
        >
          <Menu.Item key={`${path}/product/list`} icon={<UserOutlined />}>
            <NavLink to={`${path}/product/list`}
            // onClick={handleReRender}
            >
              Sản phẩm
            </NavLink>
          </Menu.Item>

          <Menu.Item key={`${path}/category/list`} icon={<VideoCameraOutlined />}>
            <NavLink to={`${path}/category/list`}>
              Thể loại
            </NavLink>
          </Menu.Item>

          <Menu.Item key={`${path}/provider/list`} icon={<UploadOutlined />}>
            <NavLink to={`${path}/provider/list`}>
              Nhà cung cấp
            </NavLink>
          </Menu.Item>
          <Menu.Item key={`${path}/order/list`} icon={<ShoppingCartOutlined />}>
            <NavLink to={`${path}/order/list`}>
              Đơn hàng
            </NavLink>
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <Header
          className="site-layout-sub-header-background"
          style={{
            paddingRight: 40,
            display: 'flex',
            justifyContent: 'end',
            alignItems: 'center'
          }}>
          <Dropdown
            overlay={menu}
            onVisibleChange={handleVisibleChange}
            visible={visible}
          >
            <Avatar
              size={40}
              icon={<UserOutlined />}
              style={{ backgroundColor: '#1890ff' }}
            />
          </Dropdown>
        </Header>
        <Content style={{ margin: '24px 16px 0' }}>
          <div className="site-layout-background" style={{ padding: 24, minHeight: '100vh' }}>
            <Switch>
              <Route path={`${path}/product/list`} >
                <ProductList
                  productList={productList}
                  handleEditProduct={handleEditProduct}
                  handleEditStatus={handleEditStatus}
                />
              </Route>
              <Route path={`${path}/product/add`} >
                <ProductAddEdit
                  handleReRender={handleReRender}
                />
              </Route>
              <Route path={`${path}/product/:productId`} >
                <ProductAddEdit
                  handleReRender={handleReRender}
                />
              </Route>

              <Route path={`${path}/category/list`}>
                <CategoryList
                  categoryList={categoryList}
                  handleEditCategory={handleEditCategory}
                />
              </Route>
              <Route path={`${path}/category/add`} >
                <CategoryAddEdit
                  handleReRender={handleReRender}
                />
              </Route>
              <Route path={`${path}/category/:categoryId`} >
                <CategoryAddEdit
                  handleReRender={handleReRender}
                />
              </Route>

              <Route path={`${path}/provider/list`}>
                <ProviderList
                  providerList={providerList}
                  handleEditProvider={handleEditProvider}
                />
              </Route>
              <Route path={`${path}/provider/add`} >
                <ProviderAddEdit
                  handleReRender={handleReRender}
                />
              </Route>
              <Route path={`${path}/provider/:providerId`} >
                <ProviderAddEdit
                  handleReRender={handleReRender}
                />
              </Route>

              {/* Đơn hàng */}
              <Route path={`${path}/order/list`}>
                <OrderList
                  orderList={orderList}
                  orderBeingDelivered={orderBeingDelivered}
                  orderDeliverySuccess={orderOrderDeliverySuccess}
                  orderDeliveryCancel={orderOrderDeliveryCancel}
                  handleShowDetails={handleShowDetails}
                  handleDeliverySuccess={handleDeliverySuccess}
                  handleDeliveryCancel={handleDeliveryCancel}
                />
              </Route>
              <Route path={`${path}/order/:orderId`} >
                <OrderDetails />
              </Route>
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </Layout >
  )
}

export default Admin