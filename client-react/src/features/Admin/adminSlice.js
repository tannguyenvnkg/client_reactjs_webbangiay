const { createSlice, createSelector } = require("@reduxjs/toolkit");

const adminState = {
    loading: false,
    productList: [],
    categoryList: [],
    providerList: [],
    orderList: [],
    orderDeliveryCancel: [],
    orderDeliverySuccess: [],
    orderBeingDelivered: [],
}

const adminSlice = createSlice({
    name: "admin",
    initialState: adminState,
    reducers: {
        fetchData(state) {
            state.loading = true;
        },
        fetchDataSuccess(state) {
            state.loading = false;
        },
        fetchDataFailed(state) {
            state.loading = false;
        },

        fetchProvider(state) {
            state.loading = true;
        },
        fetchProviderSuccess(state) {
            state.loading = false;
        },
        fetchProviderFailed(state) {
            state.loading = false;
        }

        , fetchCategory(state) {
            state.loading = true;
        },
        fetchCategorySuccess(state) {
            state.loading = false;
        },
        fetchCategoryFailed(state) {
            state.loading = false;
        }

        , fetchProduct(state) {
            state.loading = true;
        },
        fetchProductSuccess(state) {
            state.loading = false;
        },
        fetchProductFailed(state) {
            state.loading = false;
        },

        // =============== Fetch order ===============
        fetchOrderList(state) {
            state.loading = true;
        },
        fetchOrderListSuccess(state) {
            state.loading = false;
        },
        fetchOrderListFailed(state) {
            state.loading = false;
        },

        fetchOrderBeingDelivered(state) {
            state.loading = true;
        },
        fetchOrderBeingDeliveredSuccess(state) {
            state.loading = false;
        },
        fetchOrderBeingDeliveredFailed(state) {
            state.loading = false;
        },

        fetchOrderDeliveryDone(state) {
            state.loading = true;
        },
        fetchOrderDeliveryDoneSuccess(state) {
            state.loading = false;
        },
        fetchOrderDeliveryDoneFailed(state) {
            state.loading = false;
        },

        fetchOrderDeliveryCancel(state) {
            state.loading = true;
        },
        fetchOrderDeliveryCancelSuccess(state) {
            state.loading = false;
        },
        fetchOrderDeliveryCancelFailed(state) {
            state.loading = false;
        },

        updateDeliverySuccess(state){},
        updateDeliveryCancel(state){},

        //================= Set Data ===================
        setProductList(state, action) {
            state.productList = action.payload
        },
        setCategoryList(state, action) {
            state.categoryList = action.payload
        },
        setProviderList(state, action) {
            state.providerList = action.payload
        },
        setOrderList(state, action) {
            state.orderList = action.payload
        },
        setOrderBeingDelivered(state, action) {
            state.orderBeingDelivered = action.payload
        },
        setOrderDeliverySuccess(state, action) {
            state.orderDeliverySuccess = action.payload
        },
        setOrderDeliveryCancel(state, action) {
            state.orderDeliveryCancel = action.payload
        },

        // product
        insertProduct(state, action) {
            state.loading = true;
        },
        insertProductSuccess(state, action) {
            state.loading = false;
        },
        insertProductFailed(state, action) {
            state.loading = false;
        },

        updateProduct(state, action) {
            state.loading = true;
        },
        updateProductSuccess(state, action) {
            state.loading = false;
        },
        updateProductFailed(state, action) {
            state.loading = false;
        },

        updateImage(state, action) {
            state.loading = true;
        },
        updateImageSuccess(state, action) {
            state.loading = false;
        },
        updateImageFailed(state, action) {
            state.loading = false;
        },

        insertSize(state, action) {
            state.loading = true;
        },
        insertSizeSuccess(state, action) {
            state.loading = false;
        },
        insertSizeFailed(state, action) {
            state.loading = false;
        },

        // category
        insertCategory(state, action) {
            state.loading = true;
        },
        insertCategorySuccess(state, action) {
            state.loading = false;
        },
        insertCategoryFailed(state, action) {
            state.loading = false;
        },
        updateCategory(state, action) {
            state.loading = true;
        },
        updateCategorySuccess(state, action) {
            state.loading = false;
        },
        updateCategoryFailed(state, action) {
            state.loading = false;
        },


        // Provider
        insertProvider(state, action) {
            state.loading = true;
        },
        insertProviderSuccess(state, action) {
            state.loading = false;
        },
        insertProviderFailed(state, action) {
            state.loading = false;
        },

        updateProvider(state, action) {
            state.loading = true;
        },
        updateProviderSuccess(state, action) {
            state.loading = false;
        },
        updateProviderFailed(state, action) {
            state.loading = false;
        },


    },
});

// Actions
export const adminActions = adminSlice.actions;

// Selectors
export const selectLoading = (state) => state.admin.loading
export const selectCategoryList = (state) => state.admin.categoryList
export const selectProviderList = (state) => state.admin.providerList
export const selectProductList = (state) => state.admin.productList
export const selectOrderList = (state) => state.admin.orderList
export const selectOrderDeliverySuccess = (state) => state.admin.orderDeliverySuccess
export const selectOrderDeliveryCancel = (state) => state.admin.orderDeliveryCancel
export const selectOrderBeingDelivered = (state) => state.admin.orderBeingDelivered

export const selectCategoryOptions = createSelector(selectCategoryList, (categoryList) => (
    categoryList.map(category => (
        {
            label: category.categoryName,
            value: category._id
        }
    ))
))
export const selectProviderOptions = createSelector(selectProviderList, (providerList) => (
    providerList.map(provider => (
        {
            label: provider.providerName,
            value: provider._id
        }
    ))
))



// Reducer
const adminReducer = adminSlice.reducer;
export default adminReducer;