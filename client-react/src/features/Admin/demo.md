# page admin:


1. PRODUCT
    - List:
        + path: /admin/product/list
        + product
        + product by category id        
        + product by provider id

    - Add:
        + path: /admin/product/add
        + product

    - Edit:
        + path: /admin/product/:productId
        + product
        + image product  
        + size product

    # values:
        - image :
        - productName :
        - price :
        - category :
        - provider :

2. CATEGORY
    - List:
        + path: /admin/category/list
        + category
    - Add:
        + path: /admin/category/list
        + category
    - Edit:
        + path: /admin/category/:categoryId
        + category
    # values:
        - categoryName :


3. PROVIDER
    - List:
        + path: /admin/provider/list
        + provider
    - Add:
        + path: /admin/provider/add
        + provider
    - Edit:
    + path: /admin/provider/:productId
        + provider
    # values:
        - providerName : 
        - address : 
        - phone : 


4. USER
    - List:
    + path: /admin/user/list
    # values:
        - customerName: 
        - phone: 
        - address: 
        - email:
        - password: 


5. CART
    - List:
        + order list