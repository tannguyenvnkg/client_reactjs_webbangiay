import { Button, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function CategoryList(props) {

  const categoryList = props.categoryList;
  const handleEditCategory = props.handleEditCategory
  const [data, setData] = useState([])

  const columns = [
    {
      title: 'Thể loại',
      dataIndex: 'categoryName',
      key: 'categoryName',
      align: "center",
      width: '80%'
    },
    {
      title: '',
      key: 'action',
      render: (text, record) => (
        <Space>
          {categoryList.length > 0
            ? <>
              {/* Edit */}
              <Button
                style={{ width: 120, marginTop: 10 }}
                type="primary"
                onClick={() => handleEditCategory(record.key)}>Chỉnh sửa</Button>
            </>
            : null}
        </Space>
      ),
      align: "center",
    },
  ];


  useEffect(() => {
    const data = categoryList.map((item, index) => ({
      key: item._id,
      categoryName: item.categoryName,
    }))
    setData(data)
  }, [])

  return (
    <div>
      <div>
        <Link to={`add`}>
          <Button
            type='primary'
            style={{ float: 'right' }}
          >Thêm
          </Button>
        </Link>
      </div>
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  )
}

export default CategoryList