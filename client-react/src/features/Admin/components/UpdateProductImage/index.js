import { Button, Col, Form, Image, Row } from 'antd'
import { UploadFile } from 'components/FormFields'
import React from 'react'
import { useForm } from 'react-hook-form'

function UpdateProductImage({ initialValues, onSubmit, onChange }) {
  const {
    control,
    handleSubmit,
  } = useForm({
    defaultValues: initialValues,
  })

  const handleFormSubmit = async formValues => {
    try {
      await onSubmit?.(formValues);
    } catch (e) {
      console.log('Failed :', e)
    }
  }

  return (
    <div style={{ width: 600 }}>
      <Row justify="center">
        <Col>
          <Image
            width={300}
            src={initialValues.imageProduct}
          />
        </Col>
        <Col>
          <Form onFinish={handleSubmit(handleFormSubmit)}>
            <Form.Item // UploadFile
              wrapperCol={{
                offset: 3,
              }}>
              <UploadFile
                name="imageProduct"
                control={control}
                onChange={onChange}
              />
            </Form.Item>
            <Form.Item // Button submit
              wrapperCol={{
                offset: 10
              }}
            >
              <Button htmlType="submit">Chỉnh sửa</Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default UpdateProductImage