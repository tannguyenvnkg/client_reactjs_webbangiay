import { Button, Form } from 'antd';
import { InputField } from 'components/FormFields';
import React from 'react'
import { adminActions } from 'features/Admin/adminSlice';
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux';
import { mess } from 'constants/global';

export default function ProviderForm({ initialValues, onSubmit, isEdit }) {
    const {
        control,
        handleSubmit,
    } = useForm({
        defaultValues: initialValues,
    })


    const handleFormSubmit = async (formValues) => {
        try {
            await onSubmit?.(formValues);
        } catch (e) {
            console.log('Failed :', e)
        }
    }

    return (
        <div style={{ width: 600 }}>
            <Form
                onFinish={handleSubmit(handleFormSubmit)}
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
            >
                <Form.Item
                    label="Tên nhà cung cấp"
                    rules={[{ required: true, message: mess.provider.productName }]}
                >
                    <InputField name="providerName" control={control} />
                </Form.Item>
                <Form.Item
                    label="Địa chỉ"
                    rules={[{ required: true, message: mess.provider.productName }]}
                >
                    <InputField name="address" control={control} />
                </Form.Item>
                <Form.Item
                    label="Số điện thoại"
                    rules={[{ required: true, message: mess.provider.productName }]}
                >
                    <InputField name="phone" control={control} />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        offset: 10
                    }}
                >
                    <Button htmlType="submit">{isEdit ? 'Chỉnh sửa' : 'Thêm'}</Button>
                </Form.Item>
            </Form>
        </div>
    )
}
