import { Col, PageHeader, Row } from 'antd'
import CartTable from 'features/Cart/components/CartTable'
import { selectOrderDetails, userActions } from 'features/User/userSlice';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { formatCash } from 'utils';

function OrderDetails(props) {
  const { orderId } = useParams();
  const dispatch = useDispatch()
  const selectedOrderDetails = useSelector(selectOrderDetails)
  const [cartOrderList, setCartOrderList] = useState([]);
  const [orderDetails, setOrderDetails] = useState(undefined);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalMoney, setTotalMoney] = useState(0);

  // const orderDetails = props.orderDetails;
  useEffect(() => {
    dispatch(userActions.fetchOrderDetails({
      orderId: orderId,
    }))
  }, [])

  useEffect(() => {
    selectedOrderDetails && 
    setOrderDetails(selectedOrderDetails)
    setCartOrderList(orderDetails?.cart)
    setTotalMoney(orderDetails?.totalMoney)
    setTotalAmount(orderDetails?.cart.reduce((total, item) =>
    total + (Number(item.detail.amount)), 0))
    console.log(orderDetails?.cart && totalAmount, totalMoney);
  },[selectedOrderDetails, orderDetails])


  return (
    <div className="cart-container">
      <Row justify="center">
        <Col span={24}>
          <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            subTitle="Danh sách đơn hàng"
          />
          {cartOrderList ? 
          <CartTable
            cartOrderList={cartOrderList}
          /> 
          : <span />}

        </Col>
        <Col span={9}></Col>
        {cartOrderList ? 
        <Col span={9}>
          <div className="cart-order">
            <div className="cart-order__price">
              <div className="title">Tổng thanh toán ( {totalAmount}  sản phẩm ) : </div>
              <div className="price">{formatCash(totalMoney)}</div>
            </div>
          </div>
        </Col> 
         :<span />}
      </Row>

    </div>
  )
}

export default OrderDetails