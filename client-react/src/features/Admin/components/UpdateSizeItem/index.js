import { Button, Col, Form, InputNumber, Row } from 'antd';
import { InputField } from 'components/FormFields';
import { mess } from 'constants/global';
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'

function UpdateSizeItem({ initialValues, onSubmit}) {
  const {
    control,
    handleSubmit,
  } = useForm({
    defaultValues: initialValues,
  })
  const [size, setSize] = useState(0)
  const [quantity, setQuantity] = useState(0)

  useEffect(() => {
    if (initialValues) {
      setQuantity(initialValues.quantity)
      setSize(initialValues.size)
    }
  }, [initialValues])
  const handleFormSubmit = async formValues => {
    try {
      await onSubmit?.(formValues, size, quantity);
    } catch (e) {
      console.log('Failed :', e)
    }
  }

  const handleSetSize = (e) => {
    setSize(e)
  }

  const handleSetQuantity = (e) => {
    setQuantity(e)
  }
  return (
    <div style={{ width: 600 }}>
      <Row justify="center">
        <Col span={20}>
          <Form
            onFinish={handleSubmit(handleFormSubmit)}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
          >
            <Form.Item
              label="Size"
            >
              <InputNumber
                min={1}
                name="size"
                control={control}
                defaultValue={size}
                style={{ width: 200 }}
                onChange={(e) => handleSetSize(e)}
                value={size}
              />
            </Form.Item>
            <Form.Item
              label="Số lượng"
            >
              <InputNumber
                min={0}
                name="quantity"
                control={control}
                defaultValue={quantity}
                style={{ width: 200 }}
                onChange={(e) => handleSetQuantity(e)}
                value={quantity}
              />
            </Form.Item>
            <Form.Item // Button submit
              wrapperCol={{
                offset: 10
              }}
            >
              <Button htmlType="submit">Chỉnh sửa</Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default UpdateSizeItem