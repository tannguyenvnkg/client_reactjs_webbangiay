import { PageHeader } from 'antd';
import { adminActions, selectProviderList } from 'features/Admin/adminSlice';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import ProviderForm from '../ProviderForm';

function ProviderAddEdit(props) {
  const { providerId } = useParams();
  const isEdit = Boolean(providerId);
  const history = useHistory()
  const dispatch = useDispatch()
  const providerList = useSelector(selectProviderList);
  const [provider, setProvider] = useState(undefined);
  const handleReRender = props.handleReRender
  useEffect(() => {
    if (!providerId) return;
    try {
      const item = providerList.find(item => item._id.toString() === providerId);
      setProvider(item)
    } catch (error) {
      console.log('Failed', error);
    }
  }, [providerId])

  const initialValues = {
    providerName: '',
    address: '',
    phone: '',
    ...provider
  }

  const handleProviderFormSubmit = async (formValues) => {
    console.log('Submit', formValues);
    if (isEdit) {
      dispatch(adminActions.updateProvider({
        _id: formValues._id,
        providerName: formValues.providerName,
        address: formValues.address,
        phone: formValues.phone
      }))
    } else {
      dispatch(adminActions.insertProvider({
        providerName: formValues.providerName,
        address: formValues.address,
        phone: formValues.phone
      }))
    }
    history.push('/a_m_p_main/home/provider/list')
    await handleReRender()
  }

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => window.history.back()}
        subTitle="Danh sách thể loại"
      />
      <h1>{isEdit ? 'CHỈNH SỬA NHÀ CUNG CẤP' : 'THÊM NHÀ CUNG CẤP'}</h1>
      <div>
        {(!isEdit || Boolean(provider)) && (
          <div>
            <ProviderForm
              initialValues={initialValues}
              onSubmit={handleProviderFormSubmit}
              isEdit={isEdit}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default ProviderAddEdit