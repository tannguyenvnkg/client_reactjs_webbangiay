import { Button, Form, InputNumber } from 'antd';
import { InputField, SelectField, UploadFile } from 'components/FormFields';
import { mess } from 'constants/global';
import { selectCategoryOptions, selectProviderOptions } from 'features/Admin/adminSlice';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';

export default function ProductForm({ initialValues, onSubmit, isEdit, onChange }) {
    const {
        control,
        handleSubmit,
    } = useForm({
        defaultValues: initialValues,
    })

    const categoryOptions = useSelector(selectCategoryOptions)
    const providerOptions = useSelector(selectProviderOptions)

    const [detailList, setDetailList] = useState([]);

    useEffect(() => {
        if (initialValues && initialValues.details) {
            setDetailList(initialValues.details)
        }
    }, [initialValues])

    const handleFormSubmit = async formValues => {
        try {
            await onSubmit?.(formValues, detailList);
        } catch (e) {
            console.log('Failed :', e)
        }
    }

    const handleQuantity = (e, item) => {

        const newDetailList = [...detailList]
        const index = detailList.findIndex((inx) => inx.sizeId === item)
        const editedItem = {
            ...newDetailList[index],
            quantityInStock: parseInt(e)
        }
        newDetailList[index] = editedItem
        setDetailList(newDetailList)
    }

    const handleSize = (e, item) => {
        const newDetailList = [...detailList]
        const index = detailList.findIndex((inx) => inx.sizeId === item)
        const editedItem = {
            ...newDetailList[index],
            size: parseInt(e)
        }
        newDetailList[index] = editedItem
        setDetailList(newDetailList)
    }
    return (
        <div style={{ width: 600 }}>
            <Form
                onFinish={handleSubmit(handleFormSubmit)}
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}>
                <Form.Item // Product Name
                    label="Tên sản phẩm"
                    rules={[{ required: true, message: mess.product.productName }]}
                >
                    <InputField name="productName" control={control} />
                </Form.Item>
                <Form.Item // Price
                    label="Giá"
                    rules={[{ required: true, message: mess.product.price }]}
                >
                    <InputField name="price" control={control} />
                </Form.Item>
                <Form.Item // Category
                    label="Thể loại"
                    rules={[{ required: true, message: mess.product.category }]}
                >
                    <SelectField
                        onChange={(e) => console.log(e)}
                        name="categoryId"
                        control={control}
                        options={categoryOptions} />
                </Form.Item>
                <Form.Item // Provider
                    label="Nhà cung cấp"
                    rules={[{ required: true, message: mess.product.provider }]}
                >
                    <SelectField
                        name="providerId"
                        control={control}
                        label="Nhà cung cấp"
                        options={providerOptions}
                    />
                </Form.Item>
                {!isEdit ?
                    <Form.Item // UploadFile
                        wrapperCol={{
                            offset: 3,
                        }}>
                        <UploadFile
                            name="imageProduct"
                            control={control}
                            onChange={onChange}
                        />
                    </Form.Item> :
                    null}

                {isEdit ?
                    <Form.Item  // Details
                        label="Size giày"
                    >
                        <div name="details" >
                            {detailList && detailList.map((item, inx) => (
                                <div key={inx}>
                                    <InputNumber 
                                        control={control}
                                        min={0}
                                        defaultValue={item.size}
                                        value={item.size}
                                        onChange={(e) => handleSize(e, item.sizeId)}
                                    />
                                    <InputNumber
                                        control={control}
                                        min={0}
                                        addonAfter={'Đôi'}
                                        defaultValue={item.quantityInStock}
                                        value={item.quantityInStock}
                                        onChange={(e) => handleQuantity(e, item.sizeId)}
                                    />
                                </div>

                            ))
                            }
                        </div>
                    </Form.Item> :
                    null}

                <Form.Item // Button submit
                    wrapperCol={{
                        offset: 10
                    }}
                >
                    <Button htmlType="submit">{isEdit ? 'Chỉnh sửa' : 'Thêm'}</Button>
                </Form.Item>
            </Form>
        </div>
    )
}
