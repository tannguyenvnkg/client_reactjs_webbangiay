import { Button, Popconfirm, Space, Table } from 'antd';
import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

function ProviderList(props) {
  let { path, url } = useRouteMatch();
  const providerList = props.providerList;
  const handleEditProvider = props.handleEditProvider;

  const columns = [
    {
      title: 'Tên nhà cung cấp',
      dataIndex: 'providerName',
      key: 'providerName',
      align: "center",
      width: '30%'
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      key: 'address',
      align: "center",
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
      align: "center",
    },
    {
      title: '',
      key: 'action',
      render: (_, record) => (
        <Space>
          {providerList.length > 0
            ? <>
              {/* Edit */}
              <Button
                style={{ width: 120, marginTop: 10 }}
                type="primary"
                onClick={() => handleEditProvider(record.key)}>Chỉnh sửa</Button>
            </>
            : null}
        </Space>
      ),
      align: "center",
    },
  ];

  const data = providerList.map((item, index) => ({
    key: item._id,
    providerName: item.providerName,
    address: item.address,
    phone: item.phone,
  }))

  return (
    <div>
      <div>
        <Link to={`add`}>
          <Button
            type='primary'
            style={{ float: 'right' }}
          >Thêm
          </Button>
        </Link>
      </div>
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  )
}

export default ProviderList