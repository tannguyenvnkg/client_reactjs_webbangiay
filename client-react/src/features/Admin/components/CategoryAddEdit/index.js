import { PageHeader } from 'antd';
import { adminActions, selectCategoryList } from 'features/Admin/adminSlice';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import CategoryForm from '../CategoryForm';

function CategoryAddEdit(props) {

  const { categoryId } = useParams();
  const isEdit = Boolean(categoryId);
  const dispatch = useDispatch()
  const history = useHistory()
  const categoryList = useSelector(selectCategoryList);
  const [category, setCategory] = useState(undefined);
  const handleReRender = props.handleReRender
  
  useEffect(() => {
    if (!categoryId) return;

    const item = categoryList.find(item => item._id.toString() === categoryId);
    setCategory(item);
  }, [categoryId])

  const initialValues = {
    categoryName: '',
    ...category
  }

  const handleCategoryFormSubmit = async (formValues) => {
    if (isEdit) {
      dispatch(adminActions.updateCategory({
        _id: formValues._id,
        categoryName: formValues.categoryName,
      }))
    } else {
      dispatch(adminActions.insertCategory({
        categoryName: formValues.categoryName,
      }))
    }
    history.push('/a_m_p_main/home/category/list')
    await handleReRender()
  }

  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => window.history.back()}
        subTitle="Danh sách thể loại"
      />
      <h1>{isEdit ? 'CẬP NHẬT THỂ LOẠI' : 'THÊM THỂ LOẠI'}</h1>
      <div>
        {(!isEdit || Boolean(category)) && (
          <div>
            <CategoryForm
              initialValues={initialValues}
              onSubmit={handleCategoryFormSubmit}
              isEdit={isEdit}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default CategoryAddEdit