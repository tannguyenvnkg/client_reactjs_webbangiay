import { Button, Form } from 'antd';
import { InputField } from 'components/FormFields';
import { mess } from 'constants/global';
import { adminActions } from 'features/Admin/adminSlice';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';

export default function CategoryForm({ initialValues, onSubmit, isEdit }) {
    const {
        control,
        handleSubmit,
    } = useForm({
        defaultValues: initialValues,
    })

    const handleFormSubmit = async (formValues) => {
        try {
            await onSubmit?.(formValues);
        } catch (e) {
            console.log('Failed :', e)
        }
    }

    return (
        <div style={{ width: 600 }}>
            <Form
                onFinish={handleSubmit(handleFormSubmit)}
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
            >
                <Form.Item
                    label="Tên thể loại"
                    rules={[{ required: true, message: mess.category.categoryName }]}
                >
                    <InputField name="categoryName" control={control} />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        offset: 10
                    }}>
                    <Button htmlType="submit">{isEdit ? 'Chỉnh sửa' : 'Thêm'}</Button>
                </Form.Item>
            </Form>
        </div>
    )
}
