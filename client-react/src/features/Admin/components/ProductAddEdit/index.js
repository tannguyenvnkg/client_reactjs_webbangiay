import { Col, message, PageHeader, Row, Table, Tabs } from 'antd';
import axios from 'axios';
import { adminActions, selectProductList, selectProviderList } from 'features/Admin/adminSlice';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import ProductForm from '../ProductForm';
import UpdateProductImage from '../UpdateProductImage';
import UpdateSizeItem from '../UpdateSizeItem';

function ProviderAddEdit(props) {
  
  const { TabPane } = Tabs;
  const { productId } = useParams();
  const history = useHistory();
  const dispatch = useDispatch()
  const productList = useSelector(selectProductList);

  const isEdit = Boolean(productId);
  const handleReRender = props.handleReRender
  
  const [product, setProduct] = useState(undefined);
  const [progress, setProgress] = useState(0);
  const [selectedFile, setSelectedFile] = useState(null);

  useEffect(() => {
    if (!productId) return;
    dispatch(adminActions.fetchProduct())
  }, [])
  
  useEffect(() => {
    productList && 
    setProduct(productList.find(item => item._id.toString() === productId));
    console.log(product);
  },[productList])

  const initialValues = {
    productName: '',
    imageProduct: '',
    price: '',
    status: false,
    details: [],
    categoryId: product?.category?._id,
    providerId: product?.provider?._id,
    size: 1,
    quantity: 0,
    ...product
  }

  const handleUpdateSize = async (formValues, size, quantity) => {
    if(size && quantity){
      dispatch(adminActions.insertSize({
        _id: initialValues._id,
        size: size,
        quantityInStock: quantity
      }))
      await handleReRender()
    }else{
      message.error('Size phải lớn hơn 0')
    }
  }

  const handleProductFormSubmit = async (formValues, detail) => {
    if (isEdit) {
      dispatch(adminActions.updateProduct({
        _id: formValues._id,
        productName: formValues.productName,
        price: formValues.price,
        categoryId: formValues.categoryId,
        providerId: formValues.providerId,
        status: formValues.status,
        details: detail,
      }))
      history.push('/a_m_p_main/home/product/list')
      await handleReRender()
    } else {
      handleAddProduct(formValues)
    }
  }

  const handleUpdateImageSubmit = async (formValues) => {
    handleUploadImage()
  }

  const handleUploadImage = async () => {
    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: event => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        // onProgress({ percent: (event.loaded / event.total) * 100 });
      }
    };

    fmData.append("image", selectedFile, selectedFile.name);
    fmData.append("_id", initialValues._id);

    try {
      const res = await axios.put(
        `http://localhost:5000/api/product/updateImageProduct`,
        fmData,
        config
      );
      if (res.data.error) {
        message.error(res.data.message);
      } else {
        message.success(res.data.message);
        history.push('/a_m_p_main/home/product/list')
        await handleReRender()
      }
    } catch (err) {
      message.error(err);
    }
  }

  const handleAddProduct = async (formValues) => {
    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: event => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        // onProgress({ percent: (event.loaded / event.total) * 100 });
      }
    };

    fmData.append("image", selectedFile, selectedFile.name);
    fmData.append("productName", formValues.productName);
    fmData.append("price", formValues.price);
    fmData.append("category", formValues.categoryId);
    fmData.append("provider", formValues.providerId);

    try {
      const res = await axios.post(
        `http://localhost:5000/api/product/addProduct`,
        fmData,
        config
      );
      if (res.data.error) {
        message.error(res.data.message);
      } else {
        message.success(res.data.message);
        history.push('/a_m_p_main/home/product/list')
        await handleReRender()
      }
    } catch (err) {
      console.log("error: ", err);
    }
  }

  const handleOnChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };


  return (
    <div>
      <PageHeader
        className="site-page-header"
        onBack={() => window.history.back()}
        subTitle="Danh sách thể loại"
      />
      <h1>{isEdit ? 'CẬP NHẬT SẢN PHẨM' : 'THÊM SẢN PHẨM'}</h1>
      <div>
        <Row>
          <Col >
            <Tabs
              className="tab-info-user"
              type="card"
              defaultActiveKey="1"
              tabPosition='right'
            >
              <TabPane tab="Thông tin sản  phẩm" key="1">
                {(!isEdit || Boolean(product)) && (
                  <div>
                    <ProductForm
                      isEdit={isEdit}
                      initialValues={initialValues}
                      onSubmit={handleProductFormSubmit}
                      onChange={handleOnChange}
                    />
                  </div>
                )}
              </TabPane>
              {(isEdit || Boolean(product)) ?
                <>
                  <TabPane tab="Hình ảnh" key="2">
                    <UpdateProductImage
                      initialValues={initialValues}
                      onSubmit={handleUpdateImageSubmit}
                      onChange={handleOnChange}
                    />
                  </TabPane>
                  <TabPane tab="Size giày" key="3">
                    <UpdateSizeItem
                      initialValues={initialValues}
                      onSubmit={handleUpdateSize}
                    />
                  </TabPane>
                </> :
                null
              }

            </Tabs>
          </Col>
        </Row>

      </div>
    </div>
  )
}

export default ProviderAddEdit