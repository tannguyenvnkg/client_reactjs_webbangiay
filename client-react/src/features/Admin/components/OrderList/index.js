import { Button, Popconfirm, Table, Tabs } from 'antd';
import React from 'react';
import { formatCash, formatMoment } from 'utils';

function OrderList(props) {
  const { TabPane } = Tabs;
  const orderBeingDelivered = props.orderBeingDelivered;
  const orderDeliverySuccess = props.orderDeliverySuccess;
  const orderDeliveryCancel = props.orderDeliveryCancel;

  const handleShowDetails = props.handleShowDetails;
  const handleDeliverySuccess = props.handleDeliverySuccess;
  const handleDeliveryCancel = props.handleDeliveryCancel;
  // const [data, setData] = useState([])

  const columns = [
    {
      title: 'Tên khách hàng',
      dataIndex: 'nameCustomer',
      key: 'nameCustomer',
      align: "center",
      width: '25%'
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phoneCustomer',
      key: 'phoneCustomer',
      align: "center",
      width: '25%'
    },
    {
      title: 'Ngày đặt hàng',
      dataIndex: 'orderDate',
      key: 'orderDate',
      align: "center",
      width: '25%'
    },
    {
      title: 'Tổng tiền',
      dataIndex: 'totalMoney',
      key: 'totalMoney',
      align: "center",
      width: '25%'
    },
    {
      title: '',
      key: 'action',
      render: (text, record) => (
        <>
          <Button style={{ width: 120 }}
            type='primary'
            onClick={() => handleShowDetails(record.key)}
          >
            Xem chi tiết
          </Button>
          <br />
          <Popconfirm
            okText='Xác nhận'
            cancelText='Hủy'
            title="Xác nhận giao hàng?"
            onConfirm={() => handleDeliverySuccess(record.key)}>
            <Button
              style={{
                width: 120,
                marginTop: 10
              }}
            >Đã giao</Button>
          </Popconfirm>

          <br />
          <Popconfirm
            okText='Xác nhận'
            cancelText='Hủy'
            title="Xác nhận hủy đơn hàng?"
            onConfirm={() => handleDeliveryCancel(record.key)}>
            <Button
              danger
              style={{
                width: 120,
                marginTop: 10
              }}
            >Hủy đơn hàng</Button>
          </Popconfirm>
        </>
      ),
      align: "center",
    },
  ];

  const columnsCus = [
    {
      title: 'Tên khách hàng',
      dataIndex: 'nameCustomer',
      key: 'nameCustomer',
      align: "center",
      width: '25%'
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phoneCustomer',
      key: 'phoneCustomer',
      align: "center",
      width: '25%'
    },
    {
      title: 'Ngày đặt hàng',
      dataIndex: 'orderDate',
      key: 'orderDate',
      align: "center",
      width: '25%'
    },
    {
      title: 'Tổng tiền',
      dataIndex: 'totalMoney',
      key: 'totalMoney',
      align: "center",
      width: '25%'
    },
    {
      title: '',
      key: 'action',
      render: (text, record) => (
        <>
          <Button style={{ width: 120 }}
            type='primary'
            onClick={() => handleShowDetails(record.key)}
          >
            Xem chi tiết
          </Button>
        </>
      ),
      align: "center",
    },
  ];

  const dataOrderBeingDelivered = orderBeingDelivered?.map(item => ({
    key: item._id,
    nameCustomer: item.nameCustomer,
    phoneCustomer: item.phoneCustomer,
    orderDate: formatMoment(item.orderDate, 'DD-MM-YYYY'),
    totalMoney: formatCash(item.totalMoney),
  }));

  const dataOrderDeliverySuccess = orderDeliverySuccess?.map(item => ({
    key: item._id,
    nameCustomer: item.nameCustomer,
    phoneCustomer: item.phoneCustomer,
    orderDate: formatMoment(item.orderDate, 'DD-MM-YYYY'),
    totalMoney: formatCash(item.totalMoney),
  }))

  const dataOrderDeliveryCancel = orderDeliveryCancel?.map(item => ({
    key: item._id,
    nameCustomer: item.nameCustomer,
    phoneCustomer: item.phoneCustomer,
    orderDate: formatMoment(item.orderDate, 'DD-MM-YYYY'),
    totalMoney: formatCash(item.totalMoney),
  }))

  return (
    <div>
      <Tabs
        className="tab-info-user"
        type="card"
        defaultActiveKey="1"
        tabPosition='top'
      >
        <TabPane tab="Đơn hàng đang giao" key="1">
          <Table columns={columns} dataSource={dataOrderBeingDelivered} />
        </TabPane>
        <TabPane tab="Đơn hàng đã giao" key="2">
          <Table columns={columnsCus} dataSource={dataOrderDeliverySuccess} />
        </TabPane>
        <TabPane tab="Đơn hàng đã hủy" key="3">
          <Table columns={columnsCus} dataSource={dataOrderDeliveryCancel} />
        </TabPane>
      </Tabs>
    </div>
  )
}

export default OrderList