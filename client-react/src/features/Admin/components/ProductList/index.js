import React from 'react'
import { Table, Tag, Space, Image, Popconfirm, Button } from 'antd';
import { adminActions } from 'features/Admin/adminSlice';
import { formatCash } from 'utils';
import { Link } from 'react-router-dom';

function ProductList(props) {

  const productList = props.productList;
  const handleEditProduct = props.handleEditProduct
  const handleEditStatus = props.handleEditStatus
  const columns = [
    {
      title: 'Hình ảnh',
      dataIndex: 'imageProduct',
      key: 'imageProduct',
      render: url => (
        <Image
          width={150}
          src={url}
        />
      ),
      align: "center",
    },
    {
      title: 'Sản phẩm',
      dataIndex: 'productName',
      key: 'productName',
      align: "center",
      width: '20%'
    },
    {
      title: 'Giá',
      dataIndex: 'price',
      key: 'price',
      align: "center",
    },
    {
      title: 'Nhà cung cấp',
      dataIndex: 'provider',
      key: 'provider',
      align: "center",
    },
    {
      title: 'Thể loại',
      dataIndex: 'category',
      key: 'category',
      align: "center",
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      align: "center",
    },
    {
      title: '',
      key: 'action',
      render: (text, record) => (
        <>
          {productList.length > 0
            ? <>
              {/* remove */}
              <Popconfirm
                okText='Xác nhận'
                cancelText='Hủy'
                title="Thay đổi trạng thái sản phẩm?"
                onConfirm={() => handleEditStatus(record.key)}>
                <Button>Sửa trạng thái</Button>
              </Popconfirm>
              <br />
              {/* Edit */}
              <Button style={{ width: 120, marginTop: 10 }} type="primary" onClick={() => handleEditProduct(record.key)}>Chỉnh sửa</Button>
            </>
            : null}
        </>
      ),
      align: "center",
    },
  ];

  const data = productList.map((item, index) => ({
    key: item._id,
    productName: item.productName,
    imageProduct: item.imageProduct,
    price: formatCash(item.price),
    provider: item.provider.providerName,
    category: item.category.categoryName,
    status: !item.status ? 'Hết hàng' : 'Đang mở bán'
  }))

  return (
    <div>
      <div>
        <Link to={`add`}>
          <Button
            type='primary'
            style={{ float: 'right' }}
          >Thêm
          </Button>
        </Link>
      </div>
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  )
}

export default ProductList