import { takeLatest, fork, take, all, call, put } from 'redux-saga/effects'
import { adminActions } from './adminSlice';
import ProductApi from 'api/productApi'
import CategoryApi from 'api/categoryApi';
import ProviderApi from 'api/providerApi';
import { message } from 'antd';
import { push } from 'connected-react-router';
import CartApi from 'api/cartApi';


function* handleInsertProduct(payload) {
    const data = yield call(ProductApi.addProduct, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.insertProductFailed());
    } else {
        message.success(data.message);
        yield put(adminActions.insertProductSuccess());
        yield put(push('/a_m_p_main/home/product/list'))
    }
}

function* handleInsertSize(payload) {
    const data = yield call(ProductApi.addSizeProduct, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.insertSizeFailed());
    } else {
        message.success(data.message);
        yield put(adminActions.insertSizeSuccess());
    }
}

function* handleUpdateProduct(payload) {
    const data = yield call(ProductApi.updateProduct, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.updateProductFailed())
    } else {
        message.success(data.message);
        yield put(adminActions.updateProductSuccess())
        // yield put(push('/a_m_p_main/home/product/list'))
    }
}

function* handleUpdateDeliverySuccess(payload) {
    const data = yield call(CartApi.deliverySuccess, payload);
    if (data.error) {
        message.error(data.message);
        // yield put(adminActions.updateProductFailed())
    } else {
        message.success(data.message);
        // yield put(adminActions.updateProductSuccess())
        // yield put(push('/a_m_p_main/home/product/list'))
    }
}

function* handleUpdateDeliveryCancel(payload) {
    const data = yield call(CartApi.deliveryCancel, payload);
    if (data.error) {
        message.error(data.message);
        // yield put(adminActions.updateProductFailed())
    } else {
        message.success(data.message);
        // yield put(adminActions.updateProductSuccess())
        // yield put(push('/a_m_p_main/home/product/list'))
    }
}

function* handleInsertCategory(payload) {
    const data = yield call(CategoryApi.add, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.insertCategoryFailed())
    } else {
        message.success(data.message);
        yield put(adminActions.insertCategorySuccess())
        yield put(push('/a_m_p_main/home/category/list'))
    }
}
function* handleInsertProvider(payload) {
    const data = yield call(ProviderApi.add, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.insertProviderFailed())
    } else {
        message.success(data.message);
        yield put(adminActions.insertProviderSuccess())
        yield put(push('/a_m_p_main/home/provider/list'))
    }
}

function* handleUpdateCategory(payload) {
    const data = yield call(CategoryApi.update, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.updateCategoryFailed())
    } else {
        message.success(data.message);
        yield put(adminActions.updateCategorySuccess())
        yield put(push('/a_m_p_main/home/category/list'))
    }
}

function* handleUpdateProvider(payload) {
    const data = yield call(ProviderApi.update, payload);
    if (data.error) {
        message.error(data.message);
        yield put(adminActions.updateProviderFailed())
    } else {
        message.success(data.message);
        yield put(adminActions.updateProviderSuccess())
        yield put(push('/a_m_p_main/home/provider/list'))
    }
}

function* fetchProductList() {
    const { data } = yield call(ProductApi.getAll);

    yield put(adminActions.setProductList(data));
}

function* fetchCategoryList() {
    const { data } = yield call(CategoryApi.getAll);

    yield put(adminActions.setCategoryList(data));
}

function* fetchProviderList() {
    const { data } = yield call(ProviderApi.getAll);

    yield put(adminActions.setProviderList(data));
}

function* fetchOrderList() {
    const { data } = yield call(CartApi.getAll);
    yield put(adminActions.setOrderList(data));
}

function* fetchOrderBeingDelivered() {
    const { data } = yield call(CartApi.getOrderBeingDelivered);
    yield put(adminActions.setOrderBeingDelivered(data));
}

function* fetchOrderDeliverySuccess() {
    const { data } = yield call(CartApi.getOrderDeliverySuccess);
    yield put(adminActions.setOrderDeliverySuccess(data));
}

function* fetchOrderDeliveryCancel() {
    const { data } = yield call(CartApi.getOrderDeliveryCancel);
    yield put(adminActions.setOrderDeliveryCancel(data));
}

function* fetchAdminData() {
    try {
        yield all([
            call(fetchOrderList),
            call(fetchCategoryList),
            call(fetchProductList),
            call(fetchProviderList),
            call(fetchOrderDeliverySuccess),
            call(fetchOrderDeliveryCancel),
            call(fetchOrderBeingDelivered),
        ])
        yield put(adminActions.fetchDataSuccess());
    } catch (error) {
        console.log("failed to fetch home data", error);
        yield put(adminActions.fetchDataFailed());
    }
}

function* watchUpdateCategory() {
    while (true) {
        const actions = yield take(adminActions.updateCategory.type)
        yield fork(handleUpdateCategory, actions.payload);
    }
}

function* watchUpdateProvider() {
    while (true) {
        const actions = yield take(adminActions.updateProvider.type)
        yield fork(handleUpdateProvider, actions.payload);
    }
}

function* watchInsertProvider() {
    while (true) {
        const actions = yield take(adminActions.insertProvider.type)
        yield fork(handleInsertProvider, actions.payload);
    }
}

function* watchInsertCategory() {
    while (true) {
        const actions = yield take(adminActions.insertCategory.type)
        yield fork(handleInsertCategory, actions.payload);
    }
}

function* watchInsertProduct() {
    while (true) {
        const actions = yield take(adminActions.insertProduct.type)
        yield fork(handleInsertProduct, actions.payload);
    }
}
function* watchInsertSize() {
    while (true) {
        const actions = yield take(adminActions.insertSize.type)
        yield fork(handleInsertSize, actions.payload);
    }
}

function* watchUpdateProduct() {
    while (true) {
        const actions = yield take(adminActions.updateProduct.type)
        yield fork(handleUpdateProduct, actions.payload);
    }
}

function* watchUpdateDeliverySuccess() {
    while (true) {
        const actions = yield take(adminActions.updateDeliverySuccess.type)
        yield fork(handleUpdateDeliverySuccess, actions.payload);
    }
}

function* watchUpdateDeliveryCancel() {
    while (true) {
        const actions = yield take(adminActions.updateDeliveryCancel.type)
        yield fork(handleUpdateDeliveryCancel, actions.payload);
    }
}

export default function* adminSaga() {
    yield takeLatest(adminActions.fetchData.type, fetchAdminData);
    yield takeLatest(adminActions.fetchCategory.type, fetchCategoryList);
    yield takeLatest(adminActions.fetchProduct.type, fetchProductList);
    yield takeLatest(adminActions.fetchProvider.type, fetchProviderList);
    yield takeLatest(adminActions.fetchOrderList.type, fetchOrderList);

    //  category
    yield fork(watchInsertCategory)
    yield fork(watchUpdateCategory)

    //  product
    yield fork(watchInsertSize)
    yield fork(watchInsertProduct)
    yield fork(watchUpdateProduct)

    // provider
    yield fork(watchInsertProvider)
    yield fork(watchUpdateProvider)

    // cart
    yield fork(watchUpdateDeliverySuccess)
    yield fork(watchUpdateDeliveryCancel)
}