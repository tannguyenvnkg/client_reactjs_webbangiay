import React from 'react';
import { useHistory } from 'react-router-dom';
import Slider from "react-slick";
import CategoryItem from '../CategoryItem';

function Categories({ categoryList }) {
    const history = useHistory()
    var settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        rows: 2,
    };

    const handleSelectCategory = (categoryId) => {
        history.push(`product_menu/product_by_category_id/${categoryId}`)
    }

    return (
        <>
            <div className="home-item__title">THỂ LOẠI</div>
            <Slider {...settings}>
                {categoryList?.map(category => (
                    <CategoryItem
                        handleSelectCategory={handleSelectCategory}
                        key={category._id}
                        category={category}
                    />
                ))}
            </Slider>
        </>
    )
}

export default Categories