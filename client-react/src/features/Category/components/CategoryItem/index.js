import React from 'react'
import { Card } from 'antd';

function CategoryItem(props) {
  const category = props.category;
  const handleSelectCategory = props.handleSelectCategory
  const { Meta } = Card;

  return (
    <Card
      hoverable
      style={{
        width: "100%",
        textAlign: "center"
      }}
      onClick={() => handleSelectCategory(category._id)}
    >
      <Meta
        title={category.categoryName}
      />
    </Card>
  )
}

export default CategoryItem