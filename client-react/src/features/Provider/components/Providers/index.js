import React, { useState, useEffect } from 'react'
import Slider from "react-slick";
import ProviderItem from '../ProviderItem'
import ProviderApi from 'api/providerApi'
import { useHistory } from 'react-router-dom';

function Providers({ providerList }) {
    const history = useHistory()
    var settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,

        rows: 2,
        initialSlide: 0,
    };

    const handleSelectProvider = (providerId) => {
        history.push(`product_menu/product_by_provider_id/${providerId}`)
    }

    return (
        <>
            <div className="home-item__title">NHÀ CUNG CẤP</div>
            <div>
                <Slider {...settings}>
                    {providerList?.map(provider => (
                        <ProviderItem
                            handleSelectProvider={handleSelectProvider}
                            key={provider._id}
                            provider={provider}
                        />
                    ))}
                </Slider>
            </div>
        </>
    )
}

export default Providers