import React from 'react'
import { Card } from 'antd';


function ProviderItem(props) {
  const provider = props.provider;
  const handleSelectProvider = props.handleSelectProvider
  const { Meta } = Card;
  return (
    <Card
      hoverable
      style={{
        width: '100%',
        textAlign: 'center'
      }}
      onClick={() => handleSelectProvider(provider._id)}
    >
      <Meta
        title={provider.providerName}
        description={provider.address} />
    </Card>
  )
}

export default ProviderItem