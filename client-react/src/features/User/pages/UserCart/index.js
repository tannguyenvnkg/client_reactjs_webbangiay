import { CURRENT_USER } from 'constants/global';
import OrderDetails from 'features/User/components/OrderDetails';
import OrderList from 'features/User/components/OrderList'
import { selectOrderDetails, selectOrderList, userActions } from 'features/User/userSlice';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { Route } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import '../../User.scss'
export default function UserCart() {
  const dispatch = useDispatch()
  const history = useHistory()
  let { path, url } = useRouteMatch();
  const selectedOrderList = useSelector(selectOrderList)
  const [orderList, setOrderList] = useState([])

  // User
  useEffect(() => {
    const user = localStorage.getItem(CURRENT_USER) !== null
      ? JSON.parse(localStorage.getItem(CURRENT_USER))
      : [];
    dispatch(userActions.fetchOrderList({
      customerEmail: user.email
    }))
  }, [])

  useEffect(() => {
    setOrderList(selectedOrderList)
  }, [selectedOrderList])

  const handleShowDetails = async (orderId) => {
    history.push(`${path}/${orderId}`)
  }

  return (
    <div>
      <Switch>
        <Route exact path={`${path}`}>
          <OrderList
            orderList={orderList}
            handleShowDetails={handleShowDetails}
          />
        </Route>
        <Route path={`${path}/:orderId`} >
          <OrderDetails
          />
        </Route>
      </Switch>
    </div>
  )
}
