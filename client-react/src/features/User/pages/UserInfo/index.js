import { Col, message, Row, Tabs } from 'antd';
import { CURRENT_USER } from 'constants/global';
import { authActions } from 'features/Auth/authSlice';
import ChangePassword from 'features/User/components/ChangePassword';
import UserInfoForm from 'features/User/components/UserInfoForm';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { formatMoment, MD5 } from 'utils';
export default function UserInfo() {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [isEdit, setIsEdit] = useState(false);
  const [currentUser, setCurrentUser] = useState(undefined)
  const dispatch = useDispatch()
  const { TabPane } = Tabs;
  // User
  useEffect(() => {
    const user = localStorage.getItem(CURRENT_USER) !== null
      ? JSON.parse(localStorage.getItem(CURRENT_USER))
      : [];
    setCurrentUser(user);
  }, []);

  const initialValues = {
    customerName: '',
    phone: '',
    birthday: '',
    address: '',
    email: '',
    ...currentUser
  }

  const initialValuesChangePassword = {
    oldPassword: '',
    newPassword: '',
    newPasswordConfirm: '',
    ...currentUser
  }

  const handleFormSubmit = async formValues => {
    let user = {
      _id: formValues._id,
      customerName: formValues.customerName,
      phone: formValues.phone,
      birthday: formatMoment(formValues.birthday),
      address: formValues.address,
    }
    let userData = {
      _id: formValues._id,
      customerName: formValues.customerName,
      phone: formValues.phone,
      birthday: formatMoment(formValues.birthday),
      address: formValues.address,
      email: formValues.email
    }
    dispatch(authActions.update({user, userData}))
  }

  const handleChangePassword = async formValues => {
    if (formValues.oldPassword &&
      formValues.newPassword &&
      formValues.newPasswordConfirm &&
      formValues.newPassword === formValues.newPasswordConfirm) {
      let user = {
        _id: formValues._id,
        oldPassword: MD5(formValues.oldPassword),
        newPassword: MD5(formValues.newPassword),
      }
      dispatch(authActions.changePassword(user))
    } else {
      message.error('Kiểm tra lại mật khẩu!')
    }
  }

  const handleIsEdit = () => {
    setIsEdit(true);
  }

  const handleIsNotEdit = () => {
    setIsEdit(false);
  }

  return (
    <div
      style={{
        backgroundColor: '#f0f2f5',
        minHeight: '100vh',
        paddingTop: 30,
        paddingBottom: 30
      }}>

      <Row justify="center">
        <Col span={11}>
          <Tabs 
          className="tab-info-user" 
          type="card" 
          defaultActiveKey="1"
          >
            <TabPane tab="Thông tin khách hàng" key="1">
              {Boolean(currentUser) && (
                <UserInfoForm
                  isEdit={isEdit}
                  initialValues={initialValues}
                  onSubmit={handleFormSubmit}
                  onIsEdit={handleIsEdit}
                  onIsNotEdit={handleIsNotEdit}
                />
              )}
            </TabPane>
            <TabPane tab="Đổi mật khẩu" key="2">
              <ChangePassword
                initialValues={initialValuesChangePassword}
                onSubmit={handleChangePassword}
              />
            </TabPane>
          </Tabs>
        </Col>

      </Row>

    </div>
  )
}
