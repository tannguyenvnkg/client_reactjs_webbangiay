import { message } from 'antd';
import CartApi from 'api/cartApi';
import { takeLatest, call, take, put, fork } from 'redux-saga/effects'
import { userActions } from './userSlice';



function* handleFetchOrderList(payload) {
    try {
        const data = yield call(CartApi.getOrderByEmail, payload);
        if (data.error) {
            // message.error(data.message);
            yield put(userActions.fetchOrderListFailed());
        } else {
            // message.success(data.message);
            yield put(userActions.fetchOrderListSuccess());
            yield put(userActions.setOrderList(data.data))
        }
    } catch (error) {
        yield put(userActions.fetchOrderListFailed());
    }
}

function* handleFetchOrderDetails(payload) {
    try {
        const data = yield call(CartApi.getOrderById, payload);
        if (data.error) {
            // message.error(data.message);
            yield put(userActions.fetchOrderDetailsFailed());
        } else {
            // message.success(data.message);
            yield put(userActions.fetchOrderDetailsSuccess());
            yield put(userActions.setOrderDetails(data.data))
        }
    } catch (error) {
        yield put(userActions.fetchOrderDetailsSuccess());
    }
}

function* watchFetchOrderFlow() {
    while (true) {
        const action = yield take(userActions.fetchOrderList.type);
        yield fork(handleFetchOrderList, action.payload);
    }
}

function* watchFetchOrderDetailsFlow() {
    while (true) {
        const action = yield take(userActions.fetchOrderDetails.type);
        yield fork(handleFetchOrderDetails, action.payload);
    }
}

export default function* userSaga() {
    yield fork(watchFetchOrderFlow);
    yield fork(watchFetchOrderDetailsFlow);
}