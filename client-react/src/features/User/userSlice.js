const { createSlice } = require("@reduxjs/toolkit")

const userState = {
    loading: false,
    orderList: [],
    orderDetails: undefined,
}

const userSlice = createSlice({
    name: 'user',
    initialState: userState,
    reducers: {
        fetchOrderList(state) {
            state.loading = true;
        },
        fetchOrderListSuccess(state) {
            state.loading = false;
        },
        fetchOrderListFailed(state) {
            state.loading = false;
        },

        fetchOrderDetails(state) {
            state.loading = true;
        },
        fetchOrderDetailsSuccess(state) {
            state.loading = false;
        },
        fetchOrderDetailsFailed(state) {
            state.loading = false;
        },

        setOrderList(state, action) {
            state.orderList = action.payload
        },
        setOrderDetails(state, action){
            state.orderDetails = action.payload
        }
    }
})

// Action
export const userActions = userSlice.actions;

// Selectors
export const selectLoading = (state) => state.user.loading
export const selectOrderList = (state) => state.user.orderList
export const selectOrderDetails = (state) => state.user.orderDetails

// Reducers
const userReducer = userSlice.reducer
export default userReducer