import { Button, Checkbox, Image, InputNumber, Modal } from 'antd'
import { cartActions } from 'features/Cart/cartSlice'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { formatCash } from 'utils'
import { WarningFilled } from '@ant-design/icons';

function DetailItem(props) {
    const cartItem = props.cartItem

    const totalPrice = cartItem.detail.amount * cartItem.price

    return (
        <tr className="cart-table__row">
            <td className="cart-table__item item__info">
                <div className="item__info-image">
                    <Image
                        width={110}
                        src={cartItem.imageProduct}
                    />
                </div>
                <div className="item__info-title">
                    {`${cartItem.productName} ( size ${cartItem.detail.size} )`}
                </div>

            </td>
            <td className="cart-table__item item__price">
                {formatCash(cartItem.price)}
            </td>
            <td className="cart-table__item item__amount">
                {cartItem.detail.amount}
            </td>
            <td className="cart-table__item item__total">
                {formatCash(totalPrice)}
            </td>
        </tr>
    )
}

export default DetailItem