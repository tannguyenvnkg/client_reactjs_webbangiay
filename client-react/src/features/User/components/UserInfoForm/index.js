import { EditFilled } from '@ant-design/icons';
import { Button, Col, Form, Popconfirm, Row } from 'antd';
import { DatePickerField, InputField } from 'components/FormFields';
import { mess, placeholder } from 'constants/global';
import React from 'react';
import { useForm } from 'react-hook-form';

export default function UserInfoForm({ initialValues, onSubmit, isEdit, onIsEdit, onIsNotEdit }) {
    const {
        control,
        handleSubmit,
        reset
    } = useForm({
        defaultValues: initialValues,
    })
    const handleFormSubmit = async formValues => {
        try {
            await onSubmit?.(formValues);
        } catch (e) {
            console.log('Failed :', e)
        }
    }

    return (
        <div className="user-info-form">
            <Row align="middle" style={{ marginBottom: 16 }}>
                <Col offset="6">
                    <h1 className="form-header">THÔNG TIN KHÁCH HÀNG</h1>
                </Col>
                <Col offset={4}>
                    <Button onClick={onIsEdit} type="primary" icon={<EditFilled />} size="middle" />
                </Col>
            </Row>
            <Form
                onFinish={handleSubmit(handleFormSubmit)}
                labelCol={{
                    span: 7,
                }}
                wrapperCol={{
                    span: 14,
                }}>
                <Form.Item // customer Name
                    label="Tên khách hàng"
                    rules={[
                        {
                            required: true,
                            message: mess.customer.name
                        }
                    ]}
                >
                    <InputField
                        name="customerName"
                        bordered={isEdit ? true : false}
                        disabled={!isEdit}
                        control={control}
                        placeholder={placeholder.customer.name}
                    />
                </Form.Item>

                <Form.Item // email
                    label="email"
                    rules={[
                        {
                            required: true,
                            message: mess.customer.email
                        }
                    ]}
                >
                    <InputField
                        disabled
                        bordered={false}
                        name="email"
                        control={control}
                        placeholder={placeholder.customer.email}
                    />
                </Form.Item>

                <Form.Item // phone
                    label="Số điện thoại"
                    rules={[
                        {
                            required: true,
                            message: mess.customer.phone
                        }
                    ]}
                >
                    <InputField
                        name="phone"
                        bordered={isEdit ? true : false}
                        disabled={!isEdit}
                        control={control}
                        placeholder={placeholder.customer.phone}
                    />
                </Form.Item>

                <Form.Item // address
                    label="Địa chỉ"
                    rules={[
                        {
                            required: true,
                            message: mess.customer.address
                        }
                    ]}
                >
                    <InputField
                        name="address"
                        bordered={isEdit ? true : false}
                        disabled={!isEdit}
                        control={control}
                        placeholder={placeholder.customer.address}
                    />
                </Form.Item>
                <Form.Item // birthday
                    label="Ngày sinh"
                    rules={[
                        {
                            required: true,
                            message: mess.customer.birthday
                        },
                    ]}
                >
                    <DatePickerField
                        name="birthday"
                        bordered={isEdit ? true : false}
                        disabled={!isEdit}
                        control={control}
                        placeholder={placeholder.customer.birthday}
                    />
                </Form.Item>

                {isEdit ?
                    <Form.Item // Button submit
                        wrapperCol={{
                            offset: 16
                        }}
                    >
                        <Popconfirm
                            title="Xác nhận sẽ thay đổi thông tin?"
                            onConfirm={handleSubmit(handleFormSubmit)}
                            okText="Thay đổi"
                            cancelText="Hủy"
                        >
                            <Button
                                type="primary"
                                htmlType="submit"
                                style={{
                                    marginRight: 15,
                                    marginLeft: 12
                                }}>
                                Lưu
                            </Button>
                        </Popconfirm>

                        <Button
                            onClick={() => {
                                reset()
                                onIsNotEdit()
                            }} htmlType="button">Hủy</Button>
                    </Form.Item> :
                    null}
            </Form>
        </div>
    )
}
