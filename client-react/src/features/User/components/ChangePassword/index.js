import { Button, Col, Form, Popconfirm, Row } from 'antd'
import { InputField } from 'components/FormFields'
import { mess, placeholder } from 'constants/global'
import React from 'react'
import { useForm } from 'react-hook-form'

function ChangePassword({ initialValues, onSubmit }) {
  const {
    control,
    handleSubmit,
    reset
  } = useForm({
    defaultValues: initialValues,
  })

  const handleFormSubmit = async formValues => {
    try {
      await onSubmit?.(formValues);
      reset()
    } catch (e) {
      console.log('Failed :', e)
    }
  }

  return (
    <div className="user-info-form">
      <Row align="middle" style={{ marginBottom: 70 }}>
        <Col offset="9">
          <h1 className="form-header">ĐỔI MẬT KHẨU</h1>
        </Col>
      </Row>
      <Form
        onFinish={handleSubmit(handleFormSubmit)}
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 14,
        }}>
        <Form.Item // old password
          label="Mật khẩu hiện tại"
          rules={[
            {
              required: true,
              message: mess.customer.password
            }
          ]}
        >
          <InputField
            type={'password'}
            name="oldPassword"
            control={control}
            placeholder={placeholder.customer.password}
          />
        </Form.Item>
        <Form.Item // new password
          label="Mật khẩu mới"
          rules={[
            {
              required: true,
              message: mess.customer.password
            }
          ]}
        >
          <InputField
            type={'password'}
            name="newPassword"
            control={control}
            placeholder={placeholder.customer.password}
          />
        </Form.Item>

        <Form.Item // new password confirm
          label="Xác nhận mật khẩu"
          rules={[
            {
              required: true,
              message: mess.customer.passwordConfirm
            }
          ]}
        >
          <InputField
            type={'password'}
            name="newPasswordConfirm"
            control={control}
            placeholder={placeholder.customer.passwordConfirm}
          />
        </Form.Item>

        <Form.Item // Button submit
          wrapperCol={{
            offset: 19
          }}
        >
          <Popconfirm
            title="Xác nhận sẽ thay đổi mật khẩu?"
            onConfirm={handleSubmit(handleFormSubmit)}
            okText="Thay đổi"
            cancelText="Hủy"
          >
            <Button
              type="primary"
              htmlType="submit"
            >
              Lưu
            </Button>
          </Popconfirm>
        </Form.Item>
      </Form>
    </div>
  )
}

export default ChangePassword