import { Col, Row, Space, Table } from 'antd';
import React from 'react'
import { formatCash, formatMoment } from 'utils';
function OrderList(props) {
    const orderList = props.orderList
    const handleShowDetails = props.handleShowDetails

    const columns = [
        {
            title: 'Tên khách hàng',
            dataIndex: 'nameCustomer',
            key: 'nameCustomer',
            align: "center",
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phoneCustomer',
            key: 'phoneCustomer',
            align: "center",
        },
        {
            title: 'Ngày đặt hàng',
            dataIndex: 'orderDate',
            key: 'orderDate',
            align: "center",
        },
        {
            title: 'Tổng tiền',
            dataIndex: 'totalMoney',
            key: 'totalMoney',
            align: "center",
        },
        {
            title: '',
            key: 'action',
            render: (text, record) => (
                <Space>
                    <a onClick={() => handleShowDetails(record.key)}>Xem chi tiết</a>
                </Space>
            ),
            align: "center",
        },
    ];

    const data = orderList?.map(item => ({
        key: item._id,
        nameCustomer: item.nameCustomer,
        phoneCustomer: item.phoneCustomer,
        orderDate: formatMoment(item.orderDate, 'DD-MM-YYYY'),
        totalMoney: formatCash(item.totalMoney),
    }))

    return (
        <div>
            <Row justify="center">
                <Col span={18}>
                    <Table columns={columns} dataSource={data} />
                </Col>
            </Row>
        </div>
    )
}

export default OrderList