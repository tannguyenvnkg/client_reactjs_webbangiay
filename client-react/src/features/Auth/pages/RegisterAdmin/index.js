import { authActions } from 'features/Auth/authSlice';
import RegisterForm from 'features/Auth/components/RegisterForm';
import React from 'react';
import { useDispatch } from 'react-redux';
import { MD5 } from 'utils/md5';

function RegisterAdmin() {
  const dispatch = useDispatch();
  const handleFinish = (values) => {
    if (values.password === values.confirmPassword) {
      const dataRegister = {
        userName: values.userName,
        password: MD5(values.password),
        fullName: values.fullName,
      }
      dispatch(authActions.registerAdmin(dataRegister))
      console.log(dataRegister);
    } else {
      console.log('mat khau khong dung');
    }
  }
  return (
    <>
      <RegisterForm onFinish={handleFinish} />
    </>
  )
}

export default RegisterAdmin