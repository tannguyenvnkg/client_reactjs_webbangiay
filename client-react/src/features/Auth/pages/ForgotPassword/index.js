import { Col, message, Row } from 'antd'
import { EMAIL, RESET_CODE } from 'constants/global'
import { authActions } from 'features/Auth/authSlice'
import MailForm from 'features/Auth/components/MailForm'
import ReceiveOTP from 'features/Auth/components/ReceiveOTP'
import ResetPassword from 'features/Auth/components/ResetPassword'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom'
import { MD5 } from 'utils'

function ForgotPassword() {
  const { path } = useRouteMatch()
  const dispatch = useDispatch()
  const history = useHistory()
  const [Otp, setOtp] = useState("")
  const [email, setEmail] = useState("")
  
  useEffect(() =>{
    const currentEmail = localStorage.getItem(EMAIL) ? JSON.parse(localStorage.getItem(EMAIL)) : ""
    setEmail(currentEmail);
  },[])

  useEffect(() =>{
    const currentOtp = localStorage.getItem(RESET_CODE) ? JSON.parse(localStorage.getItem(RESET_CODE)) : ""
    setOtp(currentOtp);
  },[])

  // ============== Receive OTP ===============
  useEffect(() => {
    if (Otp?.length === 5) {
      dispatch(authActions.checkOtp({
        email: email,
        resetCode: Otp,
      }))
    }
  }, [Otp])

  const handleResetOtp = () => {
    setOtp("")
  }
  const handleResendOtp = () => {
    setOtp("")
    dispatch(authActions.sendEmail({
      email: email,
    }))
  }

  const handleChangeOtp = (e) => {
    setOtp(e)
  }

  // ============== Mail Form ===============
  const handleSendOTP = () => {
    // console.log(e);
    if (email.length === 0) {
      message.error('Vui lòng nhập email')
    } else {
      dispatch(authActions.sendEmail({
        email: email,
      }))
    }
  }

  const handleChangeEmail = (e) => {
    setEmail(e.target.value)
  }
  // ============== Reset Password ===============
  const handleFormSubmit = async (formValues) => {
    if (formValues.newPassword &&
      formValues.confirmNewPassword &&
      formValues.newPassword === formValues.confirmNewPassword) {
      console.log(formValues);
      dispatch(authActions.resetPassword({
        email: email,
        resetCode: Otp,
        newPassword: MD5(formValues.newPassword)
      }))
    } else {
      message.error("Kiểm tra lại mật khẩu")
    }
  }

  return (
    <Row justify="center">
      <Col span={12}>
        <div className="login-form-container">
          <h1 className="login-form-content">QUÊN MẬT KHẨU</h1>
          <Switch>
            <Route exact path={`${path}`}>
              <MailForm
                email={email}
                handleSendOTP={handleSendOTP}
                handleChangeEmail={handleChangeEmail}
              />
            </Route>
            <Route path={`${path}/receive_otp`}>
              <ReceiveOTP
                Otp={Otp}
                handleResendOtp={handleResendOtp}
                handleChangeOtp={handleChangeOtp}
                handleResetOtp={handleResetOtp}
              />
            </Route>
            <Route path={`${path}/reset_password`}>
              <ResetPassword
                onSubmit={handleFormSubmit}
              />
            </Route>
          </Switch>
        </div>
      </Col>
    </Row>
  )
}

export default ForgotPassword