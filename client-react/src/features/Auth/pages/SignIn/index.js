import { authActions } from 'features/Auth/authSlice';
import React from 'react';
import { useDispatch } from 'react-redux';
import { MD5 } from 'utils/md5';
import LoginForm from '../../components/LoginForm';

function SingIn() {
  const dispatch = useDispatch();
  const handleFinish = (values) => {
    dispatch(
      authActions.login({
        email: values.email,
        password: MD5(values.password)
      }),
      // authActions.logout()
    )
  }
  return (
    <>
      <LoginForm onFinish={handleFinish} />
    </>

  )
}

export default SingIn