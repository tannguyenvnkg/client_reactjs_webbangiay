import { authActions } from 'features/Auth/authSlice';
import LoginFormAdmin from 'features/Auth/components/LoginFormAdmin';
import React from 'react';
import { useDispatch } from 'react-redux';
import { MD5 } from 'utils/md5';

function SignInAdmin() {
    const dispatch = useDispatch();
    const handleFinish = (values) => {
        dispatch(
          authActions.loginAdmin({
            userName: values.username,
            password: MD5(values.password)
          }))
    }

    return (
        <>
            <LoginFormAdmin onFinish={handleFinish} />
        </>
    )
}

export default SignInAdmin