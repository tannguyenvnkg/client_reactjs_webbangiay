import { message } from 'antd';
import { authActions } from 'features/Auth/authSlice';
import RegisterForm from 'features/Auth/components/RegisterForm';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { MD5 } from 'utils/md5';

function Register() {
  const dispatch = useDispatch();
  const history = useHistory()
  const handleFinish = (values) => {
    if (values.password === values.confirmPassword) {
      const dataRegister = {
        customerName: values.customerName,
        phone: values.phone,
        birthday: values.birthday,
        address: values.address,
        email: values.email,
        password: MD5(values.password),
      }
      dispatch(authActions.register(dataRegister))
      history.push('/sign_in')
    } else {
      message.error("Vui lòng kiểm tra lại mật khẩu")
    }
  }
  return (
    <>
      <RegisterForm onFinish={handleFinish} />
    </>
  )
}

export default Register