import { LockOutlined, UserOutlined, MailOutlined, PhoneOutlined, HomeOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, PageHeader } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';
import { mess, placeholder } from 'constants/global';

const RegisterForm = (props) => {
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div data-aos="zoom-in">
            <Row justify="center">
                <Col span={12}>
                    <div
                        className="login-form-container">
                        <PageHeader
                            className="site-page-header"
                            onBack={() => window.history.back()}
                            subTitle="Đăng nhập"
                        />
                        <h1 className="login-form-content">ĐĂNG KÝ</h1>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                customerName: '',
                                phone: '',
                                birthday: '',
                                address: '',
                                email: '',
                                password: '',
                                confirmPassword: '',
                            }}
                            onFinish={props.onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                name="customerName"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.name,
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    // type='date'
                                    placeholder={placeholder.customer.name} />
                            </Form.Item>
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.email,
                                    },
                                ]}>
                                <Input
                                    prefix={<MailOutlined className="site-form-item-icon" />}
                                    type="email"
                                    placeholder={placeholder.customer.email}
                                />
                            </Form.Item>
                            <Form.Item
                                name="phone"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.phone,
                                    },
                                ]}>
                                <Input
                                    prefix={<PhoneOutlined className="site-form-item-icon" />}
                                    placeholder={placeholder.customer.phone}
                                />
                            </Form.Item>
                            <Form.Item
                                name="address"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.address,
                                    },
                                ]}>
                                <Input
                                    prefix={<HomeOutlined className="site-form-item-icon" />}
                                    placeholder={placeholder.customer.address}
                                />
                            </Form.Item>
                            <Form.Item
                                name="birthday"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.birthday,
                                    },
                                ]}>
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="date"
                                    placeholder={placeholder.customer.birthday}
                                />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.password,
                                    },
                                ]}>
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder={placeholder.customer.password}
                                />
                            </Form.Item>
                            <Form.Item
                                name="confirmPassword"
                                rules={[
                                    {
                                        required: true,
                                        message: mess.customer.passwordConfirm,
                                    },
                                ]}>
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder={placeholder.customer.passwordConfirm}
                                />
                            </Form.Item>

                            <div className="login-form__row-4">
                                <Form.Item>
                                    {/* <div className="login-form__item-4"> */}
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        className="login-form-button"
                                        shape='round'
                                        style={{
                                            width: '200px',
                                            height: '40px'
                                        }}
                                    >
                                        Đăng ký
                                    </Button>
                                    {/* </div> */}
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default RegisterForm