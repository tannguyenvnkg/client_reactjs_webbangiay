import { Button, Form } from 'antd';
import { InputField } from 'components/FormFields';
import { mess } from 'constants/global';
import React from 'react';
import { useForm } from 'react-hook-form';

function ResetPassword({ initialValues, onSubmit }) {
  const {
    control,
    handleSubmit,
  } = useForm({
    defaultValues: initialValues,
  })

  const handleFormSubmit = async formValues => {
    try {
      await onSubmit?.(formValues);
    } catch (e) {
      console.log('Failed :', e)
    }
  }

  return (
    <div style={{ width: 600 }}>
      <Form
        onFinish={handleSubmit(handleFormSubmit)}
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}>
        <Form.Item // new Password
          label="Mật khẩu mới"
          rules={[{ required: true, message: mess.customer.password }]}
        >
          <InputField name="newPassword" control={control} type='password'/>
        </Form.Item>
        <Form.Item // confirm New Password
          label="Xác nhận mật khẩu mới"
          rules={[{ required: true, message: mess.customer.passwordConfirm }]}
        >
          <InputField name="confirmNewPassword" control={control} type='password'/>
        </Form.Item>
        <Form.Item // Button submit
          wrapperCol={{
            offset: 12
          }}
        >
          <Button htmlType="submit">Xác nhận</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default ResetPassword