import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';

const LoginForm = (props) => {
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div data-aos="zoom-in">
            <Row justify="center">
                <Col span={12}>
                    <div
                        className="login-form-container">
                        <h1 className="login-form-content">ĐĂNG NHẬP</h1>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: false,
                                email: '',
                                password: '',
                            }}
                            onFinish={props.onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Email!',
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    type="email"
                                    placeholder="Email" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Password!',
                                    },
                                ]}>
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>

                            <div className="login-form__row-3">
                                <Form.Item style={{
                                    width: '100%'
                                }}>
                                    <div className="login-form__item-3">
                                        {/* <Form.Item name="remember" valuePropName="checked" noStyle>
                                            <Checkbox>Remember me</Checkbox>
                                        </Form.Item> */}
                                        <Link className="login-form-forgot" to="/forgot_password">
                                            Quên mật khẩu
                                        </Link>
                                    </div>
                                </Form.Item>
                            </div>
                            <div className="login-form__row-4">
                                <Form.Item>
                                    <div className="login-form__item-4">
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            className="login-form-button"
                                            shape='round'
                                            style={{
                                                width: '200px',
                                                height: '40px'
                                            }}
                                        >
                                            Đăng nhập
                                        </Button>
                                        hoặc
                                        <Link to="/register">Đăng ký ngay!</Link>
                                    </div>
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default LoginForm