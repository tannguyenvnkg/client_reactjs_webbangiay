import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Row } from 'antd';
import React from 'react';

const LoginFormAdmin = (props) => {
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div data-aos="zoom-in">
            <Row justify="center">
                <Col span={12}>
                    <div
                        className="login-form-container">
                        <h1 className="login-form-content">ĐĂNG NHẬP</h1>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: false,
                                email: '',
                                password: '',
                            }}
                            onFinish={props.onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                name="username"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập tên đăng nhập!',
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    placeholder="Tên đăng nhập" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mật khẩu!',
                                    },
                                ]}>
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Mật khẩu"
                                />
                            </Form.Item>
                            
                            <div className="login-form__row-4">
                                <Form.Item>
                                    <div className="login-form__item-4">
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            className="login-form-button"
                                            shape='round'
                                            style={{
                                                width: '200px',
                                                height: '40px'
                                            }}
                                        >
                                            Đăng nhập
                                        </Button>
                                    </div>
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default LoginFormAdmin