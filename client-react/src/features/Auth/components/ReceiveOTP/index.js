import { Button, Col, Row } from 'antd'
import OTPInput, { ResendOTP } from "otp-input-react";
import React, { useEffect, useState } from 'react'

function ReceiveOTP(props) {
    const Otp = props.Otp;
    const handleResendOtp = props.handleResendOtp
    const handleChangeOtp = props.handleChangeOtp
    const handleResetOtp = props.handleResetOtp

    useEffect(() =>{
        handleResetOtp()
    },[])
    return (
        <div>
            <div style={{
                textAlign: 'center',
                width: 450,
                margin: '0 auto'
            }}>
                <h4 style={{ textAlign: 'start' }}>Chúng tôi đã gửi mã xác nhận về Email của bạn, vui lòng kiểm tra hòm thư hoặc nếu bạn không tìm thấy tin nhắn hãy kiếm tra thư rác.</h4>
            </div>
            <OTPInput
                value={Otp}
                onChange={handleChangeOtp}
                autoFocus
                OTPLength={5}
                otpType="number"
                disabled={false}
                style={{
                    justifyContent: 'center',
                    marginTop: 10
                }}
            />

            <ResendOTP
                style={{
                    marginLeft: 'auto',
                    width: '150px',
                    justifyContent: 'space-around',
                    marginTop: 10
                }}
                onResendClick={handleResendOtp}
                maxTime={10}
            />
        </div>
    )
}

export default ReceiveOTP