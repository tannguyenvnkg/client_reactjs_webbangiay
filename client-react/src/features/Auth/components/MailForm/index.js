import { Button, Form, Input } from 'antd'
import { mess } from 'constants/global'
import React from 'react'

function MailForm(props) {
    const handleSendOTP = props.handleSendOTP
    const handleChangeEmail = props.handleChangeEmail
    const email = props.email
    return (
        <div>
            <div style={{ 
                textAlign: 'center', 
                width: 450, 
                margin: '0 auto'
                }}>
                <h3>Email của bạn là gì?</h3>
                <p  style={{ textAlign: 'start' }}>Nhập email của bạn và chúng tôi sẽ gửi mã xác thực đến, từ đó bạn có thể thay đổi mật khẩu một cách dễ dàng.</p>
            </div>
            <Form
                onFinish={handleSendOTP}
                labelCol={{
                    span: 6,
                }}
                wrapperCol={{
                    span: 16,
                }}
            >
                <Form.Item
                    label="email"
                    rules={[{ required: true, message: mess.customer.password }]}>
                    <Input 
                        type="email"
                        value={email}
                        onChange={handleChangeEmail}
                    />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                        offset: 12
                    }}>
                    <Button 
                    htmlType="submit"
                    type="primary"
                    >Gửi</Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default MailForm