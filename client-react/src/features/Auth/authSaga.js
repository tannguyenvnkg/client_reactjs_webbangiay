import { message } from 'antd';
import AdminApi from 'api/adminApi';
import UserApi from 'api/userApi';
import { push } from 'connected-react-router';
import { ACCESS_TOKEN, ACCESS_TOKEN_ADMIN, CURRENT_ADMIN, CURRENT_USER, EMAIL, RESET_CODE } from 'constants/global';
import { call, fork, put, take } from 'redux-saga/effects';
import { authActions } from './authSlice';

function* handleLogin(payload) {
    try {
        const data = yield call(UserApi.loginCustomer, payload)
        if (data.error) {
            message.error(data.messenger)
            yield put(authActions.loginFailed());
        } else {
            message.success('Đăng nhập thành công')
            yield put(authActions.loginSuccess(data.data));
            localStorage.setItem(ACCESS_TOKEN, 'true')
            localStorage.setItem(CURRENT_USER, JSON.stringify(data.data))
            yield put(push('/home'))
        }
    } catch (error) {
        yield put(
            authActions.loginFailed(error.message)
        );
    }
}


function* handleRegister(payload) {
    try {
        const data = yield call(UserApi.registerCustomer, payload);
        if (data.error) {
            message.error(data.messenger);
            yield put(authActions.registerFailed())
        } else {
            message.success(data.messenger);
            yield put(authActions.registerSuccess())
        }
    } catch (error) {
        message.error(error)
        yield put(authActions.registerFailed())
    }
}

function* handleUpload(payload) {
    try {
        const data = yield call(UserApi.updateCustomer, payload.user);
        if (data.error) {
            message.error(data.message);
            yield put(authActions.updateFailed())
        } else {
            message.success(data.message);
            yield put(authActions.updateSuccess())
            let user = JSON.parse(localStorage.getItem(CURRENT_USER));
            user = payload.userData
            localStorage.setItem(CURRENT_USER, JSON.stringify(user))
        }
    } catch (error) {
        yield put(authActions.updateFailed())
        message.error(error)
    }
}

function* handleSendEmail(payload) {
    try {
        const data = yield call(UserApi.receiveOTP, payload);
        if (data.error) {
            message.error(data.message)
            yield put(authActions.sendEmailFailed())
        } else {
            message.success(data.message)
            localStorage.setItem(EMAIL, JSON.stringify(payload.email))
            yield put(authActions.sendEmailSuccess())
            yield put(push(`/forgot_password/receive_otp`))
        }
    } catch (error) {
        yield put(authActions.sendEmailFailed())
        message.error(error)
    }
}

function* handleCheckOtp(payload) {
    try {
        const data = yield call(UserApi.checkResetCode, payload);
        if (data.error) {
            message.error(data.message)
            yield put(authActions.checkOtpFailed())
        } else {
            message.success(data.message)
            localStorage.setItem(RESET_CODE, JSON.stringify(payload.resetCode))
            yield put(authActions.checkOtpSuccess())
            yield put(push('/forgot_password/reset_password'))
        }
    } catch (error) {
        yield put(authActions.checkOtpSuccess())
        message.error(error)
    }
}

function* handleResetPassword(payload) {
    try {
        const data = yield call(UserApi.resetPassword, payload);
        if (data.error) {
            message.error(data.message)
            yield put(authActions.resetPasswordFailed())
        } else {
            message.success(data.message)
            localStorage.removeItem(RESET_CODE)
            localStorage.removeItem(EMAIL)
            yield put(push('/sign_in'))
            yield put(authActions.resetPasswordSuccess())
        }
    } catch (error) {
        yield put(authActions.resetPasswordFailed())
        message.error(error)
    }
}

function* handleChangePassword(payload) {
    try {
        const data = yield call(UserApi.changePassword, payload);
        if (data.error) {
            message.error(data.message)
            yield put(authActions.changePasswordFailed())
        } else {
            message.success(data.message)
            yield put(authActions.changePasswordSuccess())
        }
    } catch (error) {
        yield put(authActions.changePasswordFailed())
        message.error(error)
    }
}

function* handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN)
    localStorage.removeItem(CURRENT_USER)
    yield put(authActions.logout())
    yield put(push('/home'))
}


function* watchLoginFlow() {
    while (true) {
        const isLoggedIn = Boolean(localStorage.getItem(ACCESS_TOKEN))
        if (!isLoggedIn) {
            // do {
            const action = yield take(authActions.login.type);
            yield fork(handleLogin, action.payload);
            // } while (condition);
        }
        yield take(authActions.logout.type);
        yield call(handleLogout)
    }
}

function* watchRegisterFlow() {
    while (true) {
        const action = yield take(authActions.register.type);
        yield fork(handleRegister, action.payload);
    }
}

function* watchUploadFlow() {
    while (true) {
        const action = yield take(authActions.update.type);
        yield fork(handleUpload, action.payload);
    }
}

function* watchSendEmailFlow() {
    while (true) {
        const action = yield take(authActions.sendEmail.type);
        yield fork(handleSendEmail, action.payload);
    }
}

function* watchCheckOtpFlow() {
    while (true) {
        const action = yield take(authActions.checkOtp.type);
        yield fork(handleCheckOtp, action.payload);
    }
}

function* watchResetPasswordFlow() {
    while (true) {
        const action = yield take(authActions.resetPassword.type);
        yield fork(handleResetPassword, action.payload);
    }
}

function* watchChangePasswordFlow() {
    while (true) {
        const action = yield take(authActions.changePassword.type);
        yield fork(handleChangePassword, action.payload);
    }
}

//  Admin
function* watchLoginAdminFlow() {
    while (true) {
        const isLoggedIn = Boolean(localStorage.getItem(ACCESS_TOKEN_ADMIN))
        if (!isLoggedIn) {
            do {
                const action = yield take(authActions.loginAdmin.type);
                yield fork(handleLoginAdmin, action.payload);
            } while (take(authActions.loginAdminFailed.type));
        }
        yield take(authActions.logoutAdmin.type);
        yield call(handleLogoutAdmin)
    }
}

function* watchRegisterAdminFlow() {
    while (true) {
        const action = yield take(authActions.registerAdmin.type);
        yield fork(handleRegisterAdmin, action.payload);
    }
}

function* handleLoginAdmin(payload) {
    try {
        const data = yield call(AdminApi.loginAdmin, payload)
        if (data.error) {
            message.error(data.message);
            yield put(authActions.loginAdminFailed());
        } else {
            message.success(data.message);
            yield put(authActions.loginAdminSuccess());
            localStorage.setItem(ACCESS_TOKEN_ADMIN, 'true')
            localStorage.setItem(CURRENT_ADMIN, JSON.stringify(data.data))
            yield put(push('/a_m_p_main/home/product/list'))
        }
    } catch (error) {
        message.error(error)
        yield put(authActions.loginAdminFailed());
    }
}


function* handleRegisterAdmin(payload) {
    try {
        const data = yield call(AdminApi.registerAdmin, payload);
        if (data.error) {
            message.error(data.message);
            yield put(authActions.registerAdminFailed(data))
        } else {
            message.success(data.message);
            yield put(authActions.registerAdminSuccess(data))
        }
    } catch (error) {
        yield put(
            authActions.registerAdminFailed()
        )
    }
}

function* handleLogoutAdmin() {
    try {
        localStorage.removeItem(ACCESS_TOKEN_ADMIN)
        localStorage.removeItem(CURRENT_ADMIN)
        yield put(authActions.logoutAdminSuccess())
        yield put(push('/a_m_p_main/sign_in'))
    } catch {
        yield put(authActions.logoutAdminFailed())
    }

}

export default function* authSaga() {
    // User
    yield fork(watchLoginFlow)
    yield fork(watchRegisterFlow)
    yield fork(watchUploadFlow)
    yield fork(watchSendEmailFlow)
    yield fork(watchCheckOtpFlow)
    yield fork(watchResetPasswordFlow)
    yield fork(watchChangePasswordFlow)
    // Admin
    yield fork(watchLoginAdminFlow)
    yield fork(watchRegisterAdminFlow)
}