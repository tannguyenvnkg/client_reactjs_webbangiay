const { createSlice } = require("@reduxjs/toolkit");

const authState = {
    isRegistered: false,
    isLoggedIn: false,
    logging: false,
    currentUser: null,
    currentAdmin: null,
    isRegisteredAdmin: false,
    isLoggedInAdmin: false,
    isLoading: false,
}

const authSlice = createSlice({
    name: "auth",
    initialState: authState,
    reducers: {
        login(state, action) {
            state.logging = true;
        },
        loginSuccess(state, action) {
            state.isLoggedIn = true;
            state.logging = false;
            state.currentUser = action.payload;
        },
        loginFailed(state, action) {
            state.logging = false;
        },

        logout(state) {
            state.isLoggedIn = false;
            state.currentUser = null;
        },

        register(state, action) {
            state.isRegistered = false
        },
        registerFailed(state, action) {
            state.isRegistered = false
        },
        registerSuccess(state, action) {
            state.isRegistered = true
        },

        update(state, action) {
            state.isLoading = true;
        },
        updateSuccess(state, action) {
            state.isLoading = false;
        },
        updateFailed(state, action) {
            state.isLoading = false;
        },
        
        sendEmail(state, action) {
            state.isLoading = true;
        },
        sendEmailSuccess(state, action) {
            state.isLoading = false;
        },
        sendEmailFailed(state, action) {
            state.isLoading = false;
        },

        checkOtp(state, action) {
            state.isLoading = true;
        },
        checkOtpSuccess(state, action) {
            state.isLoading = false;
        },
        checkOtpFailed(state, action) {
            state.isLoading = false;
        },

        resetPassword(state, action) {
            state.isLoading = true;
        },
        resetPasswordSuccess(state, action) {
            state.isLoading = false;
        },
        resetPasswordFailed(state, action) {
            state.isLoading = false;
        },

        changePassword(state, action) {
            state.isLoading = true;
        },
        changePasswordSuccess(state, action) {
            state.isLoading = false;
        },
        changePasswordFailed(state, action) {
            state.isLoading = false;
        },

        // Admin
        loginAdmin(state, action) {
            state.logging = true;
        },
        loginAdminSuccess(state, action) {
            state.isLoggedInAdmin = true;
            state.logging = false;
            state.currentAdmin = action.payload;
        },
        loginAdminFailed(state, action) {
            state.logging = false;
        },

        logoutAdmin(state) {
            state.isLoading = true;
        },
        logoutAdminSuccess(state) {
            state.isLoggedInAdmin = false;
            state.currentAdmin = null;
            state.isLoading = false;
        },
        logoutAdminFailed(state) {
            state.isLoading = false;
        },

        registerAdmin(state, action) {
            state.isRegisteredAdmin = false
        },
        registerAdminFailed(state, action) {
            state.isRegisteredAdmin = false
        },
        registerAdminSuccess(state, action) {
            state.isRegisteredAdmin = true
        },
    }
})

// Actions
export const authActions = authSlice.actions;

// Selectors
export const selectIsLoggedIn = (state) => state.auth.isLoggedIn
export const selectIsLoggedInAdmin = (state) => state.auth.isLoggedInAdmin
export const selectIsLogging = (state) => state.auth.logging

// Reducers
const authReducer = authSlice.reducer;
export default authReducer;
