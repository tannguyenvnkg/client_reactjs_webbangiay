import CategoryApi from 'api/categoryApi';
import ProductApi from 'api/productApi';
import ProviderApi from 'api/providerApi';
import { takeLatest, put, call, all } from 'redux-saga/effects'
import { productMenuActions } from './productMenuSlice'


function* fetchProductList() {
    const { data } = yield call(ProductApi.getAll);

    yield put(productMenuActions.setProductList(data));
}

function* fetchCategoryList() {
    const { data } = yield call(CategoryApi.getAll);

    yield put(productMenuActions.setCategoryList(data));
}

function* fetchProviderList() {
    const { data } = yield call(ProviderApi.getAll);

    yield put(productMenuActions.setProviderList(data));
}

function* fetchProductMenuData() {
    try {
        yield all([
            call(fetchProductList),
            call(fetchCategoryList),
            call(fetchProviderList),
        ])
        yield put(productMenuActions.fetchDataSuccess());
    } catch (error) {
        console.log("failed to fetch home data", error);
        yield put(productMenuActions.fetchDataFailed());
    }
}

export default function* productMenuSaga() {
    yield takeLatest(productMenuActions.fetchData.type, fetchProductMenuData)
}