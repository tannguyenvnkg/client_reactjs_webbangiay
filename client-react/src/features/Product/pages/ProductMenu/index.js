import { LaptopOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import { selectProductList } from 'features/Home/homeSlice';
import AllProduct from 'features/Product/components/AllProduct';
import ProductByCategoryId from 'features/Product/components/ProductByCategoryId';
import ProductByProviderId from 'features/Product/components/ProductByProviderId';
import {
  productMenuActions,
  selectCategoryList, selectProviderList
} from 'features/Product/productMenuSlice';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Link, Route, Switch, useRouteMatch } from 'react-router-dom';


const { SubMenu } = Menu;
const { Content, Sider } = Layout;

function ProductMenu() {
  let { path, url } = useRouteMatch();

  const dispatch = useDispatch()
  const productList = useSelector(selectProductList)
  const categoryList = useSelector(selectCategoryList)
  const providerList = useSelector(selectProviderList)

  useEffect(() => {
    dispatch(productMenuActions.fetchData());
  }, [])

  return (
    <>
      <Layout style={{ minHeight: 440 }}>
        <Sider
          width={300}
          className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={'all_product'}
            style={{ height: '100%', borderRight: 0 }}
          >

            {/* All product */}
            <Menu.Item
              key="all_product">
              <Link to={`${url}`}>Danh sách sản phẩm</Link>
            </Menu.Item>

            {/* Product By Category Id */}
            <SubMenu
              key="product_by_category_id"
              // icon={<UserOutlined />}
              title="Thể loại"
            >
              {categoryList.map((category) => (
                <Menu.Item key={category._id}>
                  <Link to={`${url}/product_by_category_id/${category._id}`}>

                    {category.categoryName}
                  </Link>
                </Menu.Item>
              ))}
            </SubMenu>

            {/* Product By Provider Id */}
            <SubMenu
              key="product_by_provider_id"
              // icon={<LaptopOutlined />}
              title="Nhà cung cấp"
            >
              {providerList.map(provider => (
                <Menu.Item key={provider._id}>
                  <Link to={`${url}/product_by_provider_id/${provider._id}`}>
                    {provider.providerName}
                  </Link>
                </Menu.Item>
              ))}
            </SubMenu>
          </Menu>
        </Sider>

        {/* Layout */}
        <Layout style={{ padding: '0 24px 24px' }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            <Switch>
              <Route exact path={path} >
                <AllProduct
                  productList={productList}
                />
              </Route>
              <Route path={`${path}/product_by_category_id/:productByCategoryId`}>
                <ProductByCategoryId />
              </Route>
              <Route path={`${path}/product_by_provider_id/:productByProviderId`}>
                <ProductByProviderId />
              </Route>
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </>
  )
}

export default ProductMenu