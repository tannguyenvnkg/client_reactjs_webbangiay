
import { Col, Divider, Row } from 'antd'
import ProductAdditionalInfo from 'features/Product/components/ProductAdditionalInfo'
import ProductInfo from 'features/Product/components/ProductInfo'
import React from 'react'
import '../../Product.scss'

function ProductDetails({ product, onCloseModal }) {
    return (
        <div className='product-detail'>
            {/* dispatch 1 action, saga, => product */}
            <Row justify="center">
                <Col span={22} className="product-detail__container">

                    {/* Product Info */}
                    <ProductInfo
                        product={product}
                        onCloseModal={onCloseModal} />
                    <Divider
                        orientation="left"
                        style={{
                            fontSize: '1.8em',
                            marginTop: '30px'
                        }}
                    >
                        Thông tin bổ sung
                    </Divider>

                    {/* Additional Information  */}
                    <ProductAdditionalInfo product={product} />
                </Col>
            </Row>
        </div>
    )
}

export default ProductDetails