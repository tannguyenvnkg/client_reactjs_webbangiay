// export component
export { default as AllProduct } from './components/AllProduct'
export { default as ProductByCategoryId } from './components/ProductByCategoryId'
export { default as ProductByProviderId } from './components/ProductByProviderId'
export { default as ProductInfo } from './components/ProductInfo'
export { default as ProductItem } from './components/ProductItem'
export { default as Products } from './components/Products'

// export pages
export { default as ProductMenu } from './pages/ProductMenu'
export { default as ProductDetails } from './pages/ProductDetails'