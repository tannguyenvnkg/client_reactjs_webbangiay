import { Col, Row } from 'antd';
import CategoryApi from 'api/categoryApi';
import ProductApi from 'api/productApi';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ProductItem from '../ProductItem';

const ProductByCategoryId = () => {
  let params = useParams()

  // side effect Category List
  const [categoryData, setCategoryData] = useState([]);
  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const res = await CategoryApi.getAll();
        if (res.error === false) {
          setCategoryData(res.data);
        } else {
          console.log(res.messenger);
        }
      } catch (error) {
        console.log('Failed to fetch Categories: ', error);
      }
    }
    fetchCategories();
  }, [])

  const category = categoryData.find(category => category._id.toString() === params.productByCategoryId)
  //side effect Product by Category Id
  const [productByCategoryId, setProductByCategoryId] = useState([]);
  useEffect(() => {
    const fetchProductByCategoryId = async () => {
      try {
        const id = {
          categoryId: category._id
        }
        const res = await ProductApi.getByCategoryId(id);
        if (res.error === false) {
          setProductByCategoryId(res.data);
        } else {
          console.log(res.message);
        }
      } catch (error) {
        console.log('Failed to fetch ProductByCategoryId: ', error);
      }
    }
    fetchProductByCategoryId();
  }, [category])

  return (
    <>
      <div className="home-item__title">DANH SÁCH SẢN PHẨM</div>
      <div>
        <Row gutter={[20, 24]} justify="start">
          {(productByCategoryId.length !== 0) ?
            productByCategoryId.filter(item=> item.status === true).map(product => (
              <Col span={6} key={product._id}>
                <ProductItem
                  key={product._id}
                  product={product}
                />
              </Col>
            ))
            : 'Hiện chưa có sản phẩm này.'}
        </Row>
      </div>
    </>
  )
}

export default ProductByCategoryId