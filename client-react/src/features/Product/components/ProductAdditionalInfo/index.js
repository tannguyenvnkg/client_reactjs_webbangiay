import { Table } from 'antd'
import React from 'react'

export default function ProductAdditionalInfo({ product }) {

    const dataTable = [
        {
            key: '1',
            providerName: product.provider.providerName,
            categoryName: product.category.categoryName
        },
    ]


    const columns = [
        {
            title: 'Nhà cung cấp',
            dataIndex: 'providerName',
            key: 'providerName',
            align: 'center'
        },
        {
            title: 'Thể loại giày',
            dataIndex: 'categoryName',
            key: 'categoryName',
            align: 'center'
        },
    ]
    return (
        <div>
            <Table
                columns={columns}
                dataSource={dataTable}
                pagination={false}
                bordered={true}
                className="content-item__table"
            />
        </div>
    )
}
