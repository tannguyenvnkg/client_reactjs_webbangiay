import { selectProductList } from 'features/Product/productMenuSlice'
import React from 'react'
import { useSelector } from 'react-redux'
import Products from '../Products'

export default function AllProduct(props) {
    // const productList = props.productList
    const productList = useSelector(selectProductList)
    return (
        <>
            <Products productList={productList} />
        </>
    )
}
