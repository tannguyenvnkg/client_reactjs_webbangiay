import { Col, Row } from 'antd';
import ProductApi from 'api/productApi';
import React, { useEffect, useState } from 'react';
import ProductItem from '../ProductItem';

function Products({ productList }) {
    return (
        <>
            <div className="home-item__title">DANH SÁCH SẢN PHẨM</div>
            <div>
                <Row gutter={[20, 24]} justify="start">
                    {productList?.filter(product => product.status === true).map(product => (
                        <Col span={6} key={product._id}>
                            <ProductItem
                                key={product._id}
                                product={product}
                            />
                        </Col>
                    ))}
                </Row>
            </div>
        </>
    )
}

export default Products