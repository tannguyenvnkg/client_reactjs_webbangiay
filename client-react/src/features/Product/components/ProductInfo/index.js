import { Col, Image, Row, Button, InputNumber, Modal } from 'antd'
import { ShoppingCartOutlined } from '@ant-design/icons';
import React, { useState, useEffect } from 'react'
import '../../Product.scss'
import { useDispatch, useSelector } from 'react-redux';
import { cartActions, selectCartItems } from 'features/Cart/cartSlice';
import clsx from 'clsx';


function ProductInfo({ product, onCloseModal }) {
    const dispatch = useDispatch()
    const selectCarts = useSelector(selectCartItems)

    const [size, setSize] = useState(undefined);
    const [quantityInStock, setQuantityInStock] = useState(0);
    const [amount, setAmount] = useState(1);
    const [sizeId, setSizeId] = useState(undefined);
    const [detailList, setDetailList] = useState([]);

    
    
    useEffect(() => {
        const newDetailList = product && product.details && [...product.details]
        setDetailList(newDetailList.sort((a, b) => a.size - b.size));
    }, [product.details])
    
    useEffect(() => {
        if (detailList) {
            const availableProduct = detailList.find((item) => item.quantityInStock > 0);
            handleSelectedItem(availableProduct)
            setAmount(1)
        }
    }, [detailList])
    
    const handleSelectedItem = (item) => {
        if (item && item.quantityInStock > 0) {
            setSize(item.size)
            setQuantityInStock(item.quantityInStock)
            setSizeId(item.sizeId)
        }
    }

    const handleAmount = (value) => {
        (value > quantityInStock || value < 0) ? console.log('Số vượt quá') : setAmount(value)
    }

    const check = () => {
        if (size === undefined) {
            alert('Vui lòng chọn size!');
            return false
        }
        if (amount === 0) {
            alert('Vui lòng chọn số lượng!');
            return false
        }
        return true
    }
    const handleAddToCard = () => {
        if (check()) {
            dispatch(cartActions.addToCart({
                _id: product._id,
                size: size,
                amount: amount,
                price: product.price,
                quantityInStock: quantityInStock,
                sizeId: sizeId,
            }))
            onCloseModal()
        }
    }

    return (
        <Row justify="center">
            <Col span={10}>
                <div className='product-detail__image' >
                    <Image
                        width='100%'
                        src={product.imageProduct}
                    />
                </div>
            </Col>
            <Col span={14}>
                <div className='product-detail__content'>
                    <div className="content-item content-item__name"><h2>{product.productName}</h2></div>
                    <div className="content-item content-item__price">Giá <span className="price-item">{product.price}</span>đ</div>
                    <div className="content-item content-item__size">
                        <div className="size__text">Size</div>
                        <div className="size__table">
                            {/* {console.log(product.details)} */}
                            {detailList?.map((sizeItem, inx) => (
                                <div
                                    key={inx}
                                    onClick={() => handleSelectedItem(sizeItem)}
                                    className={
                                        clsx(
                                            "radio",
                                            size === sizeItem.size ? "selected" : "",
                                            sizeItem.quantityInStock === 0 ? "disabled" : ""
                                        )}>
                                    {sizeItem.size}
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="content-item content-item__amount">
                        <InputNumber
                            min={1}
                            max={quantityInStock}
                            // defaultValue={0}
                            value={amount}
                            onChange={(e) => handleAmount(e)}
                        >
                        </InputNumber>
                        <div className="amount-label">{quantityInStock} sản phẩm có sẵn</div>
                    </div>
                    <div className="content-item content-item__submit">
                        <Button
                            type="primary"
                            icon={<ShoppingCartOutlined className="submit-icon" />}
                            style={{
                                padding: '8px 20px',
                                height: 'auto',
                            }}
                            size='large'
                            block
                            onClick={() => handleAddToCard()}
                            disabled={amount > quantityInStock}
                        >
                            Thêm vào giỏ hàng
                        </Button>
                    </div>
                </div>
            </Col>
        </Row>
    )
}

export default ProductInfo