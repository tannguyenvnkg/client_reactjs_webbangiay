import { Col, Row } from 'antd';
import ProductApi from 'api/productApi';
import ProviderApi from 'api/providerApi';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ProductItem from '../ProductItem';

// import ProviderApi from '../../../../api/providerApi';
// import ProductApi from '../../../../api/productApi';

const ProductByProviderId = () => {
  let params = useParams()

  // side effect Provider List
  const [providerData, setProviderData] = useState([]);
  useEffect(() => {
    const fetchProviders = async () => {
      const res = await ProviderApi.getAll();
      try {
        if (res.error === false) {
          setProviderData(res.data);
        } else {
          console.log(res.message);
        }
      } catch (error) {
        console.log('Failed to fetch Providers: ', error);
      }
    }
    fetchProviders();
  }, [])

  const provider = providerData.find(provider => provider._id.toString() === params.productByProviderId)
  
  //side effect Product by Provider Id
  const [productByProviderId, setProductByProviderId] = useState([]);
  useEffect(() => {
    const fetchProductByProviderId = async () => {
      try {
        const id = {
          providerId: provider._id
        }
        const res = await ProductApi.getByProviderId(id);
        if (res.error === false) {
          setProductByProviderId(res.data);
        } else {
          console.log(res.messenger);
        }
      } catch (error) {
        console.log('Failed to fetch ProductByProviderId: ', error);
      }
    }
    fetchProductByProviderId();
  }, [provider])

  return (
    <>
      <div className="home-item__title">DANH SÁCH SẢN PHẨM</div>
      <div>
        <Row gutter={[20, 24]} justify="start">
          {(productByProviderId.length !== 0) ?
            productByProviderId?.filter(item=> item.status === true).map(product => (
              <Col span={6} key={product._id}>
                <ProductItem
                  key={product._id}
                  product={product}
                />
              </Col>
            ))
            : 'Hiện chưa có sản phẩm này.'}
        </Row>
      </div>
    </>
  )
}

export default ProductByProviderId