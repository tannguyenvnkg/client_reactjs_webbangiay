import React, { useState } from 'react'
import { Card, Modal } from 'antd';
import ProductDetails from 'features/Product/pages/ProductDetails';
import { formatCash } from 'utils';

function ProductItem({ product }) {

  const { Meta } = Card;
  const [visible, setVisible] = useState(false);

  const showModal = () => {
    setVisible(true);
  };

  const closeModal = () => {
    setVisible(false);
  }
  return (
    <>
      <div className="test" onClick={showModal}>
        <Card
          size="small"
          hoverable
          style={{ width: "100%" }}
          cover={
            <img alt={product.productName}
              src={product.imageProduct}
            // onClick={onclick}
            />
          }
        >
          <Meta
            title={product.productName}>
          </Meta>
          <p className="product-decs product-category">{product.category.categoryName}</p>
          <p className="product-decs product-price">{formatCash(product.price)}</p>
        </Card>
      </div>
      <div>
        {visible &&
          <Modal
            title="Thông tin sản phẩm"
            centered
            visible={visible}
            onOk={closeModal}
            onCancel={closeModal}
            width={1000}
            footer={null}
          >
            <ProductDetails
              product={product}
              onCloseModal={closeModal} />
          </Modal>}
      </div>
    </>
  )
}

export default ProductItem