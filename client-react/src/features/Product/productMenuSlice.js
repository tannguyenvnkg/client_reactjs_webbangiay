const { createSlice } = require("@reduxjs/toolkit")


const productMenuState = {
    loading: false,
    providerList: [],
    categoryList: [],
    productList: [],
    productByCategoryId: [],
    productByProviderId: []
}

const productMenuSlice = createSlice({
    name: 'productMenu',
    initialState: productMenuState,
    reducers: {
        fetchData(state) {
            state.loading = true;
        },
        fetchDataSuccess(state) {
            state.loading = false;
        },
        fetchDataFailed(state) {
            state.loading = false;
        },


        setProductList(state, action) {
            state.productList = action.payload
        },
        setCategoryList(state, action) {
            state.categoryList = action.payload
        },
        setProviderList(state, action) {
            state.providerList = action.payload
        },
        setProductByProviderId(state, action) {
            state.productByProviderId = action.payload
        },
        setProductByCategoryId(state, action) {
            state.productByCategoryId = action.payload
        },
    }
});

// Actions
export const productMenuActions = productMenuSlice.actions;

// Selectors
export const selectLoading = (state) => state.productMenu.loading
export const selectCategoryList = (state) => state.productMenu.categoryList
export const selectProviderList = (state) => state.productMenu.providerList
export const selectProductList = (state) => state.productMenu.productList
export const selectProductByCategoryId = (state) => state.productMenu.productByCategoryId
export const selectProductByProviderId = (state) => state.productMenu.productByProviderId


// Reducer
const productMenuReducer = productMenuSlice.reducer;
export default productMenuReducer;