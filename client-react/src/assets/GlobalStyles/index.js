import './GlobalStyles.scss'

function GlobalStyles({ children }) {
    return (
        <div className="grid__full-width">
        {children}
        </div>
    )
}

export default GlobalStyles