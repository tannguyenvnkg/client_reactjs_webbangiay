const moment = require('moment');

export const formatMoment = (value, format) => moment(value).format(format)

